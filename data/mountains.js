const mountains = [
  {
    mountain: 'Mount Everest',
    metres: '8,849',
    range: 'Himalayas',
    location: 'Nepal/China'
  },
  { mountain: 'K2', metres: '8,611', range: 'Karakoram', location: 'Pakistan' },
  {
    mountain: 'Kangchenjunga',
    metres: '8,586',
    range: 'Himalayas',
    location: 'Nepal/India'
  },
  {
    mountain: 'Lhotse',
    metres: '8,516',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Makalu',
    metres: '8,485',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Cho Oyu',
    metres: '8,188',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Dhaulagiri',
    metres: '8,167',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Manaslu',
    metres: '8,163',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Nanga Parbat',
    metres: '8,126',
    range: 'Himalayas',
    location: 'Pakistan'
  },
  {
    mountain: 'Annapurna',
    metres: '8,091',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Gasherbrum I (Hidden peak; K5)',
    metres: '8,080',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Broad Peak',
    metres: '8,051',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Gasherbrum II (K4)',
    metres: '8,035',
    range: 'Karakoram',
    location: 'Pakistan/China'
  },
  {
    mountain: 'Shishapangma',
    metres: '8,027',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Gasherbrum III',
    metres: '7,952',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Gyachung Kang',
    metres: '7,952',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)/China'
  },
  {
    mountain: 'Annapurna II',
    metres: '7,937',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Gasherbrum IV (K3)',
    metres: '7,932',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Himalchuli',
    metres: '7,893',
    range: 'Himalayas',
    location: 'Manaslu, Nepal'
  },
  {
    mountain: 'Distaghil Sar',
    metres: '7,885',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Ngadi Chuli',
    metres: '7,871',
    range: 'Himalayas',
    location: 'Manaslu, Nepal'
  },
  {
    mountain: 'Nuptse',
    metres: '7,861',
    range: 'Himalayas',
    location: 'Everest Massif, Nepal'
  },
  {
    mountain: 'Khunyang Chhish',
    metres: '7,852',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Masherbrum (K1)',
    metres: '7,821',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Nanda Devi',
    metres: '7,816',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Chomo Lonzo',
    metres: '7,804',
    range: 'Himalayas',
    location: 'Makalu Massiff, Nepal/China'
  },
  {
    mountain: 'Batura Sar',
    metres: '7,795',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kanjut Sar',
    metres: '7,790',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Rakaposhi',
    metres: '7,788',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Namcha Barwa',
    metres: '7,782',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Batura II',
    metres: '7,762',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kamet',
    metres: '7,756',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Saltoro Kangri',
    metres: '7,742',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Batura III',
    metres: '7,729',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Jannu',
    metres: '7,710',
    range: 'Himalayas',
    location: 'Kangchenjunga, Nepal'
  },
  {
    mountain: 'Tirich Mir',
    metres: '7,708',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Molamenqing',
    metres: '7,703',
    range: 'Himalayas',
    location: 'Shishapangma group, China'
  },
  {
    mountain: 'Gurla Mandhata',
    metres: '7,694',
    range: 'Himalayas (Nalakankar)',
    location: 'China'
  },
  {
    mountain: 'Saser Kangri',
    metres: '7,672',
    range: 'Karakoram',
    location: 'Pakistan / India (Jammu and Kashmir)'
  },
  {
    mountain: 'Chogolisa',
    metres: '7,665',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kongur Tagh',
    metres: '7,649',
    range: 'Pamir or Kunlun Mountains',
    location: 'China'
  },
  {
    mountain: 'Shispare',
    metres: '7,611',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Silberzacken',
    metres: '7,597',
    range: 'Himalaya',
    location: 'Pakistan'
  },
  {
    mountain: 'Changtse',
    metres: '7,583',
    range: 'Himalayas',
    location: 'Everest Massif, China'
  },
  {
    mountain: 'Trivor',
    metres: '7,577',
    range: 'Karakoram',
    location: 'Karakoram - Pakistan'
  },
  {
    mountain: 'Gangkhar Puensum',
    metres: '7,570',
    range: 'Himalayas',
    location: 'Bhutan/China'
  },
  {
    mountain: 'Gongga Shan',
    metres: '7,556',
    range: 'Daxue Shan',
    location: 'Sichuan, China'
  },
  {
    mountain: 'Annapurna III',
    metres: '7,555',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Kula Kangri',
    metres: '7,554',
    range: 'Himalayas',
    location: 'China  (possibly also Bhutan)'
  },
  {
    mountain: 'Skyang Kangri',
    metres: '7,545',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Liankang Kangri',
    metres: '7,535',
    range: 'Himalayas',
    location: 'Bhutan/China'
  },
  {
    mountain: 'Yukshin Gardan Sar',
    metres: '7,530',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Annapurna IV',
    metres: '7,525',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Saser Kangri II',
    metres: '7,518',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Mamostong Kangri',
    metres: '7,516',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Muztagh Ata',
    metres: '7,509',
    range: 'Kongur Tagh',
    location: 'China (Xinjiang)'
  },
  {
    mountain: 'Ismoil Somoni Peak',
    metres: '7,495',
    range: 'Pamir Mountains',
    location: 'Tajikistan'
  },
  {
    mountain: 'Saser Kangri III',
    metres: '7,495',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Noshaq',
    metres: '7,492',
    range: 'Hindu Kush',
    location: 'Pakistan/Afghanistan'
  },
  {
    mountain: 'Pumari Chhish',
    metres: '7,492',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Passu Sar',
    metres: '7,476',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Jongsong Peak',
    metres: '7,462',
    range: 'Himalayas',
    location: 'India/Nepal/China'
  },
  {
    mountain: 'Malubiting',
    metres: '7,458',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Gangapurna',
    metres: '7,455',
    range: 'Annapurna Himalaya',
    location: 'Nepal'
  },
  {
    mountain: 'Muchu Chhish (Batura V)',
    metres: '7,453',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Jengish Chokusu',
    metres: '7,439',
    range: 'Tian Shan',
    location: 'China/Kyrgyzstan'
  },
  {
    mountain: 'K12',
    metres: '7,428',
    range: 'Karakoram',
    location: 'India(Jammu and Kashmir)/Pakistan'
  },
  {
    mountain: 'Sia Kangri',
    metres: '7,422',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Momhil Sar',
    metres: '7,414',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Istor-o-Nal',
    metres: '7,403',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Ghent Kangri',
    metres: '7,401',
    range: 'Karakoram',
    location: 'India  (Jammu and Kashmir)/Pakistan'
  },
  {
    mountain: 'Haramosh Peak',
    metres: '7,397',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kabru',
    metres: '7,394',
    range: 'Himalayas',
    location: 'Kanchenjunga, India (Sikkim)/Nepal'
  },
  {
    mountain: 'Ultar',
    metres: '7,388',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Rimo I',
    metres: '7,385',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Sherpi Kangri',
    metres: '7,380',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Churen Himal',
    metres: '7,371',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Labuche Kang',
    metres: '7,367',
    range: 'Himalayas',
    location: 'near Cho Oyu, Nepal/China'
  },
  {
    mountain: 'Kirat Chuli',
    metres: '7,365',
    range: 'Himalayas',
    location: 'Nepal/India (Sikkim) border'
  },
  {
    mountain: 'Skil Brum',
    metres: '7,360',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Abi Gamin',
    metres: '7,355',
    range: 'Himalayas',
    location: 'India'
  },
  {
    mountain: 'Gimmigela Chuli',
    metres: '7,350',
    range: 'Himalayas',
    location: 'Nepal/India (Sikkim) border'
  },
  {
    mountain: 'Saraghrar',
    metres: '7,340',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Bojohagur Duanasir',
    metres: '7,329',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Chamlang',
    metres: '7,319',
    range: 'Himalayas',
    location: 'Khumbu, Nepal'
  },
  {
    mountain: 'Chongtar Kangri',
    metres: '7,315',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Jomolhari / Chomolhari',
    metres: '7,314',
    range: 'Himalayas',
    location: 'Bhutan/China'
  },
  {
    mountain: 'Baltoro Kangri',
    metres: '7,312',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Siguang Ri',
    metres: '7,308',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Summa Ri',
    metres: '7,302',
    range: 'Himalayas',
    location: 'Pakistan'
  },
  {
    mountain: 'The Crown',
    metres: '7,295',
    range: 'Karakoram',
    location: 'China (Xinjiang)'
  },
  {
    mountain: 'Gyala Peri',
    metres: '7,294',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Porong Ri',
    metres: '7,292',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Baintha Brakk',
    metres: '7,285',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Yutmaru Sar',
    metres: '7,283',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  { mountain: 'K6', metres: '7,282', range: 'Karakoram', location: 'Pakistan' },
  {
    mountain: 'Kangpenqing',
    metres: '7,281',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Mana Peak',
    metres: '7,272',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Muztagh Tower',
    metres: '7,273',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Diran',
    metres: '7,257',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Apsarasas Kangri',
    metres: '7,245',
    range: 'Karakoram',
    location: 'China/Pakistan'
  },
  {
    mountain: 'Langtang Lirung',
    metres: '7,227',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Karjiang',
    metres: '7,221',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Annapurna South',
    metres: '7,219',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Khartaphu',
    metres: '7,213',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Tongshanjiabu',
    metres: '7,207',
    range: 'Himalayas',
    location: 'Bhutan/China'
  },
  {
    mountain: 'Langtang Ri',
    metres: '7,205',
    range: 'Himalayas',
    location: 'Nepal/China'
  },
  {
    mountain: 'Kangphu Kang',
    metres: '7,204',
    range: 'Himalayas',
    location: 'Bhutan/China'
  },
  {
    mountain: 'Singhi Kangri',
    metres: '7,202',
    range: 'Himalayas',
    location: 'India/China'
  },
  {
    mountain: 'Lupghar Sar',
    metres: '7,200',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Gurja Himal',
    metres: '7,193',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Melungtse',
    metres: '7,181',
    range: 'Rolwaling Himalayas',
    location: 'China'
  },
  {
    mountain: 'Liushi Shan',
    metres: '7,167',
    range: 'Kunlun',
    location: 'China'
  },
  {
    mountain: 'Baruntse',
    metres: '7,162',
    range: 'Himalayas',
    location: 'Khumbu, Nepal'
  },
  {
    mountain: 'Pumori',
    metres: '7,161',
    range: 'Himalayas',
    location: 'Khumbu, Nepal'
  },
  {
    mountain: 'Hardeol',
    metres: '7,151',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Gasherbrum V',
    metres: '7,147',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Latok I',
    metres: '7,145',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kampire Dior',
    metres: '7,143',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Nemjung',
    metres: '7,140',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Udren Zom',
    metres: '7,140',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Chaukhamba',
    metres: '7,138',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Nun Kun',
    metres: '7,135',
    range: 'Karakoram',
    location: 'India(Jammu and Kashmir)'
  },
  {
    mountain: 'Tilicho Peak',
    metres: '7,134',
    range: 'Himalayas',
    location: 'Annapurna Himal, Nepal'
  },
  {
    mountain: 'Gauri Sankar',
    metres: '7,134',
    range: 'Rolwaling Himalayas',
    location: 'Nepal/China'
  },
  {
    mountain: 'Lenin Peak',
    metres: '7,134',
    range: 'Pamir Mountains',
    location: 'Tajikistan-Kyrgyzstan'
  },
  {
    mountain: 'Bularung Sar',
    metres: '7,134',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  { mountain: 'Api', metres: '7,132', range: 'Himalayas', location: 'Nepal' },
  {
    mountain: 'Teri Kang',
    metres: '7,124',
    range: 'Himalayas',
    location: 'Bhutan'
  },
  {
    mountain: 'Pauhunri',
    metres: '7,128',
    range: 'Himalayas',
    location: 'India (Sikkim)/China'
  },
  {
    mountain: 'Trisul',
    metres: '7,120',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Korzhenevskaya',
    metres: '7,105',
    range: 'Pamir Mountains',
    location: 'Tajikistan'
  },
  {
    mountain: 'Lunpo Gangri',
    metres: '7,095',
    range: 'Himalayas (Gangdise)',
    location: 'China'
  },
  {
    mountain: 'Satopanth',
    metres: '7,075',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Tirsuli',
    metres: '7,074',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Dunagiri',
    metres: '7,066',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Kangto',
    metres: '7,060',
    range: 'Himalayas',
    location: 'India (Arunachal Pradesh)/China'
  },
  {
    mountain: 'Nyegyi Kansang',
    metres: '7,047',
    range: 'Himalayas',
    location: 'India (Arunachal Pradesh)/China'
  },
  {
    mountain: 'Chomolhari Kang',
    metres: '7,046',
    range: 'Himalayas',
    location: 'Bhutan'
  },
  {
    mountain: 'Salasungo',
    metres: '7,043',
    range: 'Himalayas',
    location: 'Nepal/China'
  },
  {
    mountain: 'Link Sar',
    metres: '7,041',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kezhen Peak',
    metres: '7,038',
    range: 'Karakoram',
    location: 'China (Xinjiang)'
  },
  {
    mountain: 'Shah Dhar',
    metres: '7,038',
    range: 'Hindu Kush',
    location: 'Pakistan/Afghanistan'
  },
  {
    mountain: 'Saipal',
    metres: '7,031',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Padmanabh',
    metres: '7,030',
    range: 'Himalayas',
    location: 'India'
  },
  {
    mountain: 'Spantik',
    metres: '7,027',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Pamri Sar',
    metres: '7,016',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Khan Tengri',
    metres: '7,010',
    range: 'Tian Shan',
    location: 'Kyrgyzstan-Kazakhstan-China'
  },
  {
    mountain: 'Machapuchare',
    metres: '6,993',
    range: 'Annapurna Himalaya',
    location: 'Nepal'
  },
  {
    mountain: 'Laila Peak (Haramosh Valley)',
    metres: '6,985',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kang Guru',
    metres: '6,981',
    range: 'Manaslu Himalaya',
    location: 'Nepal'
  },
  {
    mountain: 'Gasherbrum VI',
    metres: '6,979',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Karun Kuh',
    metres: '6,977',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Avicenna Peak',
    metres: '6,974',
    range: 'Pamir Mountains',
    location: 'Tajikistan'
  },
  {
    mountain: 'Ulugh Muztagh',
    metres: '6,973',
    range: 'Kunlun Mountains',
    location: ''
  },
  {
    mountain: 'Aconcagua',
    metres: '6,962',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Napko Kangri',
    metres: '6,956',
    range: 'Karakoram',
    location: 'China'
  },
  {
    mountain: 'Sangemarmar Sar',
    metres: '6,949',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Kedarnath (mountain)',
    metres: '6,940',
    range: 'Himalayas',
    location: 'India'
  },
  { mountain: 'K7', metres: '6,934', range: 'Karakoram', location: 'Pakistan' },
  {
    mountain: 'Panchchuli',
    metres: '6,904',
    range: 'Himalayas',
    location: 'India'
  },
  {
    mountain: 'Thalay Sagar',
    metres: '6,904',
    range: 'Himalayas',
    location: 'India'
  },
  {
    mountain: 'Lunkho e Dosare',
    metres: '6,901',
    range: 'Hindu Kush',
    location: 'Afghanistan-Pakistan'
  },
  {
    mountain: 'Lunag Ri',
    metres: '6,895',
    range: 'Himalaya',
    location: 'Nepal-China'
  },
  {
    mountain: 'Ojos del Salado',
    metres: '6,891',
    range: 'Andes',
    location: 'Argentina-Chile'
  },
  {
    mountain: 'Siniolchu',
    metres: '6,888',
    range: 'Kangchenjunga Himalaya',
    location: 'India (Sikkim)'
  },
  {
    mountain: 'Kanjiroba',
    metres: '6,883',
    range: 'Himalayas',
    location: 'Nepal'
  },
  { mountain: 'Bairiga', metres: '6,882', range: 'Kangri Garpo', location: '' },
  {
    mountain: 'Koyo Zom',
    metres: '6,872',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Nanda Kot',
    metres: '6,861',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Kubi Gangri',
    metres: '6,859',
    range: 'Himalayas',
    location: 'Nepal/China'
  },
  {
    mountain: 'Angel Sar',
    metres: '6,858',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Bhagirathi Parbat I',
    metres: '6,856',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Jethi Bahurani',
    metres: '6,850',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Chongra Peak',
    metres: '6,830',
    range: 'Nanga Parbat Himalaya',
    location: 'Pakistan'
  },
  {
    mountain: 'Chomo Yummo',
    metres: '6,829',
    range: 'Sikkim',
    location: 'India/China'
  },
  {
    mountain: 'Reo Purgyil',
    metres: '6,816',
    range: 'Western Himalaya',
    location: 'India'
  },
  {
    mountain: 'Ama Dablam',
    metres: '6,812',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Monte Pissis',
    metres: '6,795',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Kangtega',
    metres: '6,782',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Biarchedi',
    metres: '6,781',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Huascarán Sur',
    metres: '6,768',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Cerro Bonete',
    metres: '6,759',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Nevado Tres Cruces',
    metres: '6,749',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Kawagarbo',
    metres: '6,740',
    range: 'Himalayas',
    location: 'China'
  },
  {
    mountain: 'Llullaillaco',
    metres: '6,739',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Cho Polu',
    metres: '6,735',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Kangju Kangri',
    metres: '6,725',
    range: 'Karakoram',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Changla',
    metres: '6,721',
    range: 'Himalayas',
    location: 'Nepal/China'
  },
  {
    mountain: 'Mercedario',
    metres: '6,720',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Mount Pandim',
    metres: '6,691',
    range: 'Himalayas',
    location: 'India (Sikkim)'
  },
  {
    mountain: 'Num Ri',
    metres: '6,677',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Lungser Kangri',
    metres: '6,666',
    range: 'Ladakh, Himalayas',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Meru Peak',
    metres: '6,660',
    range: 'Himalayas',
    location: 'India'
  },
  {
    mountain: 'Gul Lasht Zom',
    metres: '6,657',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Huascarán Norte',
    metres: '6,655',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Khumbutse',
    metres: '6,640',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)/China'
  },
  {
    mountain: 'Mount Kailash',
    metres: '6,638',
    range: 'Transhimalaya',
    location: 'China'
  },
  { mountain: 'Yerupajá', metres: '6,635', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Nevado Tres Cruces Central',
    metres: '6,629',
    range: 'Andes',
    location: 'Chile'
  },
  {
    mountain: 'Thamserku',
    metres: '6,623',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Geladaindong Peak',
    metres: '6,621',
    range: 'Tanggula',
    location: 'China (Qinghai)'
  },
  {
    mountain: 'Incahuasi',
    metres: '6,621',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Pangpoche',
    metres: '6,620',
    range: 'Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Manirang',
    metres: '6,597',
    range: 'Himalayas',
    location: 'India (Himachal Pradesh)'
  },
  {
    mountain: 'Nilkantha',
    metres: '6,596',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  {
    mountain: 'Phuparash Peak',
    metres: '6,574',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Sickle Moon Peak',
    metres: '6,574',
    range: 'Himalayas',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Tupungato',
    metres: '6,570',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Buni Zom',
    metres: '6,542',
    range: 'Hindu Raj',
    location: 'Pakistan'
  },
  {
    mountain: 'Nevado Sajama',
    metres: '6,542',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Ghamubar Zom',
    metres: '6,518',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Singu Chuli',
    metres: '6,501',
    range: 'Annapurna Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Taboche',
    metres: '6,501',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Cerro El Muerto',
    metres: '6,488',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Mera Peak',
    metres: '6,476',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Hiunchuli',
    metres: '6,441',
    range: 'Annapurna Himalaya',
    location: 'Nepal'
  },
  {
    mountain: 'Cholatse',
    metres: '6,440',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Illimani',
    metres: '6,438',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Ancohuma',
    metres: '6,427',
    range: 'Andes',
    location: 'Bolivia'
  },
  { mountain: 'Coropuna', metres: '6,425', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Antofalla',
    metres: '6,409',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Kang Yatze',
    metres: '6,400',
    range: 'Ladakh, Himalayas',
    location: 'India (Jammu and Kashmir)'
  },
  { mountain: 'Huandoy', metres: '6,395', range: 'Andes', location: 'Peru' },
  { mountain: 'Ausangate', metres: '6,384', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Chapaev Peak',
    metres: '6,371',
    range: 'Tian Shan',
    location: 'Kyrgyzstan'
  },
  { mountain: 'Illampu', metres: '6,368', range: 'Andes', location: 'Bolivia' },
  {
    mountain: 'Kusum Kangguru',
    metres: '6,367',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Kinnaur Kailash',
    metres: '6,349',
    range: 'Himalayas, Kinnaur',
    location: 'India (Himachal Pradesh)'
  },
  {
    mountain: 'Parinaquta',
    metres: '6,348',
    range: 'Andes',
    location: 'Bolivia/Chile'
  },
  {
    mountain: 'Siula Grande',
    metres: '6,344',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Bamba Dhura',
    metres: '6,334',
    range: 'Himalayas',
    location: 'India (Uttarakhand)'
  },
  { mountain: 'Ampato', metres: '6,288', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Amne Machin',
    metres: '6,282',
    range: 'Kunlun Mountains',
    location: 'China (Qinghai)'
  },
  {
    mountain: 'Pomerape',
    metres: '6,282',
    range: 'Andes',
    location: 'Bolivia/Chile'
  },
  { mountain: 'Salcantay', metres: '6,271', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Chimborazo',
    metres: '6,267',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Mount Siguniang',
    metres: '6,250',
    range: 'Qionglai Mountains',
    location: 'China (Sichuan)'
  },
  {
    mountain: 'Grid Nie Mountain',
    metres: '6,224',
    range: 'Hengduan Mountains',
    location: 'China (Sichuan)'
  },
  {
    mountain: 'Yuzhu Peak',
    metres: '6,224',
    range: 'Kunlun Mountains',
    location: 'China (Qinghai)'
  },
  {
    mountain: 'Genyen Massif',
    metres: '6,204',
    range: 'Shaluli Range',
    location: 'China (Sichuan)'
  },
  {
    mountain: 'Kongde Ri',
    metres: '6,187',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Aucanquilcha',
    metres: '6,176',
    range: 'Andes',
    location: 'Chile'
  },
  {
    mountain: 'Imja Tse',
    metres: '6,189',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Denali (Mount McKinley)',
    metres: '6,168',
    range: 'Alaska Range',
    location: 'United States (Alaska)'
  },
  {
    mountain: 'Stok Kangri',
    metres: '6,137',
    range: 'Himalayas',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Lobuche',
    metres: '6,119',
    range: 'Himalayas',
    location: 'Nepal (Khumbu)'
  },
  {
    mountain: 'Marmolejo',
    metres: '6,108',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Laila Peak (Hushe Valley)',
    metres: '6,096',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Pisang Peak',
    metres: '6,091',
    range: 'Annapurna Himalaya',
    location: 'Nepal'
  },
  {
    mountain: 'Huayna Potosí',
    metres: '6,088',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Aracar',
    metres: '6,082',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Chachakumani',
    metres: '6,074',
    range: 'Andes',
    location: 'Bolivia'
  },
  { mountain: 'Chachani', metres: '6,057', range: 'Andes', location: 'Peru' },
  { mountain: 'Mianzimu', metres: '6,054', range: 'Yunnan', location: 'China' },
  {
    mountain: 'Acotango',
    metres: '6,052',
    range: 'Andes',
    location: 'Bolivia/Chile'
  },
  {
    mountain: 'Socompa',
    metres: '6,051',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Minglik Sar',
    metres: '6,050',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Acamarachi',
    metres: '6,046',
    range: 'Andes',
    location: 'Chile'
  },
  {
    mountain: 'Shayaz',
    metres: '6,026',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Hualca Hualca',
    metres: '6,025',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Uturunku',
    metres: '6,020',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Mitre Peak',
    metres: '6,010',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Laila Peak',
    metres: '5,971',
    range: 'Himalaya',
    location: 'Pakistan'
  },
  {
    mountain: 'Mount Logan',
    metres: '5,959',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  { mountain: 'Alpamayo', metres: '5,947', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Cerro Lípez',
    metres: '5,929',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Licancabur',
    metres: '5,920',
    range: 'Andes',
    location: 'Bolivia/Chile'
  },
  {
    mountain: 'Falak Sar',
    metres: '5,918',
    range: 'Hindu Kush',
    location: 'Pakistan'
  },
  {
    mountain: 'Cotopaxi',
    metres: '5,897',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Mount Kilimanjaro',
    metres: '5,895',
    range: 'Eastern Rift Mountains',
    location: 'Tanzania'
  },
  {
    mountain: 'Hkakabo Razi',
    metres: '5,881',
    range: 'Himalayas',
    location: 'Myanmar'
  },
  { mountain: 'San José', metres: '5,856', range: 'Andes', location: 'Chile' },
  { mountain: 'El Misti', metres: '5,822', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Altun Shan',
    metres: '5,798',
    range: 'Altyn-Tagh',
    location: 'Gansu, China'
  },
  { mountain: 'Cayambe', metres: '5,790', range: 'Andes', location: 'Ecuador' },
  {
    mountain: 'Pico Cristóbal Colón',
    metres: '5,776',
    range: 'Sierra Nevada de Santa Marta',
    location: 'Colombia'
  },
  {
    mountain: 'Antisana',
    metres: '5,753',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Nevado Pisco',
    metres: '5,752',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Nevado Anallajsi',
    metres: '5,750',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Pokalde',
    metres: '5,745',
    range: 'Khumbu Himalayas',
    location: 'Nepal'
  },
  { mountain: 'Ubinas', metres: '5,672', range: 'Andes', location: 'Peru' },
  {
    mountain: 'Pichu Pichu',
    metres: '5,664',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Mount Elbrus',
    metres: '5,642',
    range: 'Caucasus Mountains',
    location: 'North Caucasus, Russia'
  },
  {
    mountain: 'Mehrbani Peak',
    metres: '5,639',
    range: 'Karakoram',
    location: 'Pakistan'
  },
  {
    mountain: 'Pico de Orizaba',
    metres: '5,636',
    range: 'Trans-Mexican Volcanic Belt',
    location: 'Mexico'
  },
  {
    mountain: 'Mount Damavand',
    metres: '5,610',
    range: 'Alborz',
    location: 'Iran'
  },
  {
    mountain: 'Nevado Mismi',
    metres: '5,597',
    range: 'Andes',
    location: 'Peru'
  },
  {
    mountain: 'Jade Dragon Snow Mountain',
    metres: '5,596',
    range: 'Hengduan Mountains',
    location: 'Yunnan, China'
  },
  {
    mountain: 'Lascar Volcano',
    metres: '5,592',
    range: 'Andes',
    location: 'Chile'
  },
  {
    mountain: 'Mount Xuebaoding',
    metres: '5,588',
    range: 'Min Mountains',
    location: 'Sichuan, China'
  },
  {
    mountain: 'Kala Patthar',
    metres: '5,545',
    range: 'Khumbu Himalayas',
    location: 'Nepal'
  },
  {
    mountain: 'Mount Saint Elias',
    metres: '5,489',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada/Alaska, US'
  },
  {
    mountain: 'Concord Peak',
    metres: '5,469',
    range: 'Pamir Mountains',
    location: 'Afghanistan-Tajikistan'
  },
  {
    mountain: 'Dindaw Razi',
    metres: '5,464',
    range: 'Himalayas',
    location: 'Myanmar'
  },
  {
    mountain: 'Machoi Peak',
    metres: '5,458',
    range: 'Himalaya',
    location: 'India (Jammu and Kashmir)'
  },
  { mountain: 'El Plomo', metres: '5,450', range: 'Andes', location: 'Chile' },
  {
    mountain: 'Bogda Feng',
    metres: '5,445',
    range: 'Tien Shan',
    location: 'Xinjiang, China'
  },
  {
    mountain: 'Mount Little Xuebaoding',
    metres: '5,443',
    range: 'Min Mountains',
    location: 'Sichuan, China'
  },
  {
    mountain: 'Cerro El Plomo',
    metres: '5,434',
    range: 'Andes',
    location: 'Chile'
  },
  {
    mountain: 'Popocatépetl',
    metres: '5,426',
    range: 'Trans-Mexican Volcanic Belt',
    location: 'Mexico'
  },
  {
    mountain: 'Kolahoi Peak',
    metres: '5,425',
    range: 'Himalaya',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Chacaltaya',
    metres: '5,421',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'Mount Pomiu',
    metres: '5,413',
    range: 'Qionglai Range',
    location: 'Sichuan, China'
  },
  {
    mountain: 'Ritacuba Blanco',
    metres: '5,410',
    range: 'Andes',
    location: 'Colombia'
  },
  {
    mountain: 'Haba Xueshan',
    metres: '5,396',
    range: 'Himalaya',
    location: 'Yunnan, China'
  },
  {
    mountain: 'Nevado del Ruiz',
    metres: '5,389',
    range: 'Andes',
    location: 'Colombia'
  },
  {
    mountain: 'Nevado del Huila',
    metres: '5,364',
    range: 'Andes',
    location: 'Colombia'
  },
  {
    mountain: 'El Altar',
    metres: '5,320',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Mount Foraker',
    metres: '5,304',
    range: 'Alaska Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Haramukh',
    metres: '5,300',
    range: 'Himalaya',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Nevado del Tolima',
    metres: '5,276',
    range: 'Andes',
    location: 'Colombia'
  },
  {
    mountain: 'Maipo',
    metres: '5,264',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Illiniza',
    metres: '5,248',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Point 5240',
    metres: '5,240',
    range: 'Himalaya',
    location: 'Jammu and Kashmir, India'
  },
  {
    mountain: 'Sirbal Peak',
    metres: '5,236',
    range: 'Himalaya',
    location: 'Kashmir Valley, India (Jammu and Kashmir)'
  },
  { mountain: 'Sangay', metres: '5,230', range: 'Andes', location: 'Ecuador' },
  {
    mountain: 'Iztaccíhuatl',
    metres: '5,230',
    range: 'Trans-Mexican Volcanic Belt',
    location: 'Mexico'
  },
  {
    mountain: 'Mount Lucania',
    metres: '5,226',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Karakol Peak',
    metres: '5,216',
    range: 'Tian Shan',
    location: 'Kyrgyzstan'
  },
  {
    mountain: 'Dykh-Tau',
    metres: '5,205',
    range: 'Caucasus Mountains',
    location: 'North Caucasus, Russia'
  },
  {
    mountain: 'Shkhara',
    metres: '5,201',
    range: 'Caucasus Mountains',
    location: 'Georgia'
  },
  { mountain: 'Mount Kenya', metres: '5,199', range: '', location: 'Kenya' },
  {
    mountain: 'Malika Parbat',
    metres: '5,190',
    range: 'Himalaya',
    location: 'Kaghan Valley, Pakistan'
  },
  {
    mountain: 'Amarnath Peak',
    metres: '5,186',
    range: 'Himalaya',
    location: 'Kashmir Valley, India (Jammu and Kashmir)'
  },
  {
    mountain: "Laram Q'awa (Charaña)",
    metres: '5,182',
    range: 'Andes',
    location: 'Bolivia'
  },
  {
    mountain: 'King Peak',
    metres: '5,173',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Boris Yeltsin Peak',
    metres: '5,168',
    range: 'Teskey Ala-Too',
    location: 'Kyrgyzstan'
  },
  {
    mountain: 'Koshtan-Tau',
    metres: '5,150',
    range: 'Caucasus Mountains',
    location: 'North Caucasus, Russia'
  },
  { mountain: 'Mount Ararat', metres: '5,137', range: '', location: 'Turkey' },
  {
    mountain: 'Mount Stanley',
    metres: '5,109',
    range: 'Ruwenzori Mountains',
    location: 'Democratic Republic of the Congo/Uganda'
  },
  {
    mountain: 'Tami Razi',
    metres: '5,101',
    range: 'Himalayas',
    location: 'Myanmar'
  },
  {
    mountain: 'Mount Steele',
    metres: '5,073',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Janga',
    metres: '5,051',
    range: 'Caucasus Mountains',
    location: 'Georgia / North Caucasus, Russia'
  },
  {
    mountain: 'Mount Kazbek',
    metres: '5,047',
    range: 'Caucasus Mountains',
    location: 'Georgia'
  },
  {
    mountain: 'Tungurahua',
    metres: '5,023',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Carihuairazo',
    metres: '5,018',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Mount Bona',
    metres: '5,005',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Blackburn',
    metres: '4,996',
    range: 'Wrangell Mtns.',
    location: 'Alaska, US'
  },
  {
    mountain: 'Pico Bolívar',
    metres: '4,981',
    range: 'Sierra Nevada de Mérida, Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Talgar Peak',
    metres: '4,979',
    range: 'Tian Shan',
    location: 'Kazakhstan'
  },
  {
    mountain: 'Shota Rustaveli Peak',
    metres: '4,960',
    range: 'Caucasus Mountains, Svaneti, Georgia/North Caucasus',
    location: 'Russia'
  },
  {
    mountain: 'Gunshar',
    metres: '4,950',
    range: 'Kohistan District',
    location: 'Pakistan'
  },
  {
    mountain: 'Mount Sanford',
    metres: '4,949',
    range: 'Wrangell Mtns.',
    location: 'Alaska, US'
  },
  {
    mountain: 'Pico Humboldt',
    metres: '4,940',
    range: 'Sierra Nevada de Mérida, Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Pin Bhaba Pass',
    metres: '4,930',
    range: 'Pin Valley/Spiti Valley, Himalayas',
    location: 'India'
  },
  {
    mountain: 'Vinson Massif',
    metres: '4,892',
    range: 'Sentinel Range, Ellsworth Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Pico Bonpland',
    metres: '4,890',
    range: 'Sierra Nevada de Mérida, Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Puncak Jaya',
    metres: '4,884',
    range: 'Sudirman Range, Papua',
    location: 'Indonesia'
  },
  {
    mountain: 'Pico La Concha',
    metres: '4,870',
    range: 'Sierra Nevada de Mérida, Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Gistola',
    metres: '4,860',
    range: 'Caucasus Mountains, Svaneti',
    location: 'Georgia'
  },
  {
    mountain: 'Tetnuldi',
    metres: '4,858',
    range: 'Caucasus Mountains, Svaneti',
    location: 'Georgia'
  },
  {
    mountain: 'Mount Tyree',
    metres: '4,852',
    range: 'Sentinel Range, Ellsworth Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Huaynaputina',
    metres: '4,850',
    range: 'Andes',
    location: 'Peru'
  },
  { mountain: 'Alam Kuh', metres: '4,850', range: 'Alborz', location: 'Iran' },
  {
    mountain: 'Mount Wood',
    metres: '4,842',
    range: 'Saint Elias Mountains, Yukon',
    location: 'Canada'
  },
  {
    mountain: 'Mount Vancouver',
    metres: '4,812',
    range: 'Saint Elias Mountains, Yukon',
    location: 'Canada'
  },
  { mountain: 'Sabalan', metres: '4,811', range: '', location: 'Iran' },
  {
    mountain: 'Mont Blanc',
    metres: '4,810',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  { mountain: 'Corazón', metres: '4,790', range: 'Andes', location: 'Ecuador' },
  {
    mountain: 'Pichincha',
    metres: '4,784',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Jimara',
    metres: '4,780',
    range: 'Khokh Range, Caucasus Mountains',
    location: 'Georgia'
  },
  {
    mountain: 'Mount Churchill',
    metres: '4,766',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Puncak Mandala',
    metres: '4,760',
    range: 'Papua',
    location: 'Indonesia'
  },
  {
    mountain: 'Klyuchevskaya Sopka',
    metres: '4,750',
    range: 'Kamchatka',
    location: 'Russia'
  },
  {
    mountain: 'Puncak Trikora',
    metres: '4,750',
    range: 'Papua',
    location: 'Indonesia'
  },
  {
    mountain: 'Mont Blanc de Courmayeur',
    metres: '4,748',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  {
    mountain: 'Sunset Peak',
    metres: '4,745',
    range: 'Himalaya, Kashmir Valley',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Mount Slaggard',
    metres: '4,742',
    range: 'Saint Elias Mountains',
    location: 'Canada'
  },
  {
    mountain: 'Pico Piedras Blancas',
    metres: '4,740',
    range: 'Sierra de la Culata, Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Pico El Toro',
    metres: '4,730',
    range: 'Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Tatakooti Peak',
    metres: '4,725',
    range: 'Himalaya, Kashmir Valley',
    location: 'India (Jammu and Kashmir)'
  },
  {
    mountain: 'Rumiñahui',
    metres: '4,721',
    range: 'Andes',
    location: 'Ecuador'
  },
  {
    mountain: 'Pico El Leon',
    metres: '4,720',
    range: 'Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Ushba',
    metres: '4,710',
    range: 'Caucasus Mountains, Svaneti',
    location: 'Georgia'
  },
  {
    mountain: 'Volcán Domuyo',
    metres: '4,709',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Pico Los Nevados',
    metres: '4,700',
    range: 'Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Pico Pan de Azucar',
    metres: '4,680',
    range: 'Sierra de la Culata, Andes',
    location: 'Venezuela'
  },
  { mountain: 'Naltar Peak', metres: '4,678', range: '', location: 'Pakistan' },
  {
    mountain: 'Mount Fairweather',
    metres: '4,663',
    range: 'Fairweather Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Pico Mucuñuque',
    metres: '4,660',
    range: 'Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Pico El Buitre',
    metres: '4,650',
    range: 'Andes',
    location: 'Venezuela'
  },
  {
    mountain: 'Khazret Sultan',
    metres: '4,643',
    range: 'Gissar Range',
    location: 'Uzbekistan'
  },
  { mountain: 'Sierra Negra', metres: '4,640', range: '', location: 'Mexico' },
  {
    mountain: 'Dufourspitze (Monte Rosa)',
    metres: '4,634',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Dunantspitze (Monte Rosa)',
    metres: '4,632',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Nordend (Monte Rosa)',
    metres: '4,609',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'Mount Hubbard',
    metres: '4,577',
    range: 'Saint Elias Mountains',
    location: ''
  },
  {
    mountain: 'Nevado de Toluca',
    metres: '4,577',
    range: '',
    location: 'Mexico'
  },
  { mountain: 'Mount Meru', metres: '4,566', range: '', location: 'Tanzania' },
  {
    mountain: 'Zumsteinspitze (Monte Rosa)',
    metres: '4,563',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'Signalkuppe (Monte Rosa)',
    metres: '4,554',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'Dom',
    metres: '4,545',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  { mountain: 'Ras Dashen', metres: '4,533', range: '', location: 'Ethiopia' },
  {
    mountain: 'Eastern Liskamm (Lyskamm)',
    metres: '4,533',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'Mount Bear',
    metres: '4,521',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Wilhelm',
    metres: '4,509',
    range: 'Bismarck Range',
    location: 'Papua New Guinea'
  },
  {
    mountain: 'Mount Karisimbi',
    metres: '4,507',
    range: 'Virunga Mountains',
    location: 'Rwanda/DRC'
  },
  {
    mountain: 'Mount Walsh',
    metres: '4,507',
    range: 'Saint Elias Mountains',
    location: 'Canada'
  },
  {
    mountain: 'Belukha Mountain',
    metres: '4,506',
    range: 'Altay Mountains',
    location: 'Russia'
  },
  {
    mountain: 'Weisshorn',
    metres: '4,506',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Tebulosmta',
    metres: '4,493',
    range: 'Caucasus Mountains',
    location: 'Georgia/Chechnya, Russia'
  },
  {
    mountain: 'Täschhorn',
    metres: '4,491',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Bazarduzu Dagi',
    metres: '4,485',
    range: '',
    location: 'Azerbaijan'
  },
  {
    mountain: 'Matterhorn',
    metres: '4,478',
    range: 'Pennine Alps',
    location: 'Switzerland/Italy'
  },
  {
    mountain: 'Mount Rutford',
    metres: '4,477',
    range: 'Sentinel Range, Ellsworth Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Mont Maudit',
    metres: '4,465',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  {
    mountain: 'Babis Mta',
    metres: '4,454',
    range: 'Caucasus Mountains',
    location: 'Georgia/Russia'
  },
  {
    mountain: 'Mount Shani',
    metres: '4,451',
    range: 'Caucasus Mountainsx',
    location: 'Georgia/Russia'
  },
  {
    mountain: 'Dena',
    metres: '4,448',
    range: 'Zagros Mountains',
    location: 'Iran'
  },
  {
    mountain: 'Vladimir Putin Peak',
    metres: '4,446',
    range: 'Kyrgyz Alatau',
    location: 'Kyrgyzstan'
  },
  {
    mountain: 'Mount Hunter',
    metres: '4,442',
    range: 'Alaska Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Parrotspitze (Monte Rosa)',
    metres: '4,432',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'La Malinche',
    metres: '4,430',
    range: 'Cordillera Neovolcanica',
    location: 'Mexico'
  },
  {
    mountain: 'Mount Whitney',
    metres: '4,421',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Alverstone',
    metres: '4,420',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US/Yukon, Canada'
  },
  {
    mountain: 'University Peak',
    metres: '4,411',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Aello Peak',
    metres: '4,403',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Elbert',
    metres: '4,402',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Massive',
    metres: '4,395',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Harvard',
    metres: '4,395',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Rainier',
    metres: '4,392',
    range: 'Cascades',
    location: 'Washington, US'
  },
  { mountain: 'Kholeno', metres: '4,387', range: 'Alborz', location: 'Iran' },
  {
    mountain: 'Mount Williamson',
    metres: '4,382',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Tavan Bogd Uul',
    metres: '4,374',
    range: 'Bayan-Ölgii Province',
    location: 'Mongolia'
  },
  {
    mountain: 'Blanca Peak',
    metres: '4,374',
    range: 'Rocky Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'La Plata Peak',
    metres: '4,372',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Giluwe',
    metres: '4,368',
    range: '',
    location: 'Papua New Guinea'
  },
  {
    mountain: 'Burchula',
    metres: '4,364',
    range: 'Caucasus Mountains',
    location: 'Georgia'
  },
  {
    mountain: 'Uncompahgre Peak',
    metres: '4,361',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Dent Blanche',
    metres: '4,357',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Crestone Peak',
    metres: '4,357',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Lincoln',
    metres: '4,354',
    range: 'Mosquito Range',
    location: 'Colorado, US'
  },
  { mountain: 'Azad Kuh', metres: '4,355', range: 'Alborz', location: 'Iran' },
  {
    mountain: 'Grays Peak',
    metres: '4,350',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Evans',
    metres: '4,350',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Quandary Peak',
    metres: '4,350',
    range: 'Tenmile Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Lalveri',
    metres: '4,350',
    range: 'Caucasus Mountains',
    location: 'Svaneti, Georgia'
  },
  {
    mountain: 'Mount Antero',
    metres: '4,349',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Torreys Peak',
    metres: '4,349',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Castle Peak',
    metres: '4,348',
    range: 'Elk Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Longs Peak',
    metres: '4,345',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'McArthur Peak',
    metres: '4,344',
    range: 'Saint Elias Mountains',
    location: 'Yukon'
  },
  {
    mountain: 'Mount Wilsion',
    metres: '4,342',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'White Mountain Peak',
    metres: '4,342',
    range: 'White Mountains',
    location: 'California, US'
  },
  {
    mountain: 'North Palisade',
    metres: '4,341',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Ludwigshöhe (Monte Rosa)',
    metres: '4,341',
    range: 'Pennine Alps',
    location: 'Switzerland-Italy'
  },
  {
    mountain: 'Mount Shavano',
    metres: '4,337',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  { mountain: 'Colima', metres: '4,330', range: '', location: 'Mexico' },
  {
    mountain: 'Crestone Needle',
    metres: '4,327',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Belford',
    metres: '4,327',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Princeton',
    metres: '4,327',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Shasta',
    metres: '4,322',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Elgon',
    metres: '4,321',
    range: '',
    location: 'Uganda-Kenya'
  },
  {
    mountain: 'Mount Bross',
    metres: '4,320',
    range: 'Mosquito Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Kit Carson Mountain',
    metres: '4,318',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Wrangell',
    metres: '4,317',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Maroon Peak',
    metres: '4,315',
    range: 'Elk Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Tabeguache Peak',
    metres: '4,315',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'El Diente Peak',
    metres: '4,315',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Sill',
    metres: '4,314',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Grand Combin',
    metres: '4,313',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Oxford (Colorado)',
    metres: '4,313',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Sneffels',
    metres: '4,312',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Democrat',
    metres: '4,312',
    range: 'Mosquito Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Capitol Peak',
    metres: '4,307',
    range: 'Elk Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Pikes Peak',
    metres: '4,302',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Russell',
    metres: '4,296',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Snowmass Mountain',
    metres: '4,295',
    range: 'Elk Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Lenzspitze',
    metres: '4,294',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Eolus',
    metres: '4,292',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Windom Peak',
    metres: '4,292',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Challenger Point',
    metres: '4,292',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Columbia',
    metres: '4,291',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Adishi',
    metres: '4,290',
    range: 'Caucasus Mountains',
    location: 'Svaneti, Georgia'
  },
  {
    mountain: 'Mount Augusta',
    metres: '4,290',
    range: 'Saint Elias Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Humboldt Peak (Colorado)',
    metres: '4,287',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Split Mountain',
    metres: '4,287',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Bierstadt',
    metres: '4,286',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Diklosmta',
    metres: '4,285',
    range: 'Caucasus Mountains',
    location: 'Tusheti, Georgia'
  },
  {
    mountain: 'Sunlight Peak',
    metres: '4,285',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Sidley',
    metres: '4,285',
    range: 'Volcanic Seven Summits',
    location: 'Antarctica'
  },
  {
    mountain: 'Missouri Mountain',
    metres: '4,282',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Culebra Peak',
    metres: '4,282',
    range: '',
    location: 'Colorado, US'
  },
  {
    mountain: 'Handies Peak',
    metres: '4,281',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Ellingwood Point',
    metres: '4,280',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Lindsey',
    metres: '4,280',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Little Bear Peak',
    metres: '4,279',
    range: 'Sangre de Cristo Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Sherman',
    metres: '4,278',
    range: 'Mosquito Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Redcloud Peak',
    metres: '4,277',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Galeras',
    metres: '4,276',
    range: 'Andes',
    location: 'Columbia'
  },
  {
    mountain: 'Mount Langley',
    metres: '4,275',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Finsteraarhorn',
    metres: '4,274',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Pyramid Peak',
    metres: '4,273',
    range: 'Elk Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Tyndall',
    metres: '4,273',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Wilson Peak',
    metres: '4,272',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Muir',
    metres: '4,272',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'San Luis Peak',
    metres: '4,271',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Wetterhorn Peak',
    metres: '4,271',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Middle Palisade',
    metres: '4,271',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Huron Peak',
    metres: '4,269',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount of the Holy Cross',
    metres: '4,269',
    range: 'Sawatch Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Thunderbolt Peak',
    metres: '4,268',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Sunshine Peak',
    metres: '4,268',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Strickland',
    metres: '4,260',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Pigeon Peak',
    metres: '4,259',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Kennedy',
    metres: '4,250',
    range: 'Saint Elias Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Pointe Bayeux',
    metres: '4,258',
    range: 'Graian Alps, Mont Blanc massif',
    location: 'France'
  },
  {
    mountain: 'Mont Blanc du Tacul',
    metres: '4,248',
    range: 'Graian Alps, Mont Blanc massif',
    location: 'France'
  },
  {
    mountain: 'Stecknadelhorn',
    metres: '4,241',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Avalanche Peak',
    metres: '4,228',
    range: 'Saint Elias Mountains, Yukon',
    location: 'Canada'
  },
  {
    mountain: 'Castor',
    metres: '4,223',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Zinalrothorn',
    metres: '4,221',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Volcán Tajumulco',
    metres: '4,220',
    range: '',
    location: 'Guatemala'
  },
  {
    mountain: 'Hohberghorn',
    metres: '4,219',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Turret Peak',
    metres: '4,217',
    range: 'Needle Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Darwin',
    metres: '4,216',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Hayes',
    metres: '4,216',
    range: '',
    location: 'Alaska, US'
  },
  { mountain: 'Sacabaya', metres: '4,215', range: '', location: 'Bolivia' },
  {
    mountain: 'Grandes Jorasses',
    metres: '4,208',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  {
    mountain: 'Gannett Peak',
    metres: '4,208',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  { mountain: 'Mauna Kea', metres: '4,207', range: '', location: 'Hawaii, US' },
  {
    mountain: 'Alphubel',
    metres: '4,206',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Cofre de Perote',
    metres: '4,201',
    range: '',
    location: 'Mexico'
  },
  {
    mountain: 'Zard-Kuh',
    metres: '4,200',
    range: 'Zagros Mountains',
    location: 'Iran'
  },
  {
    mountain: 'Shah Alborz',
    metres: '4,200',
    range: 'Alborz',
    location: 'Iran'
  },
  {
    mountain: 'Rimpfischhorn',
    metres: '4,199',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Grand Teton',
    metres: '4,199',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Cook',
    metres: '4,196',
    range: 'Saint Elias Mountains',
    location: 'Canada/US'
  },
  {
    mountain: 'Aletschhorn',
    metres: '4,192',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Strahlhorn',
    metres: '4,190',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Fremont Peak',
    metres: '4,189',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Warren',
    metres: '4,182',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Sidley',
    metres: '4,181',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Abbot',
    metres: '4,180',
    range: '',
    location: 'California, US'
  },
  {
    mountain: "Dent d'Hérens",
    metres: '4,171',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  { mountain: 'Mauna Loa', metres: '4,171', range: '', location: 'Hawaii, US' },
  {
    mountain: 'Jbel Toubkal',
    metres: '4,167',
    range: 'Atlas Mountains',
    location: 'Morocco'
  },
  {
    mountain: 'Mount Minto',
    metres: '4,165',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Breithorn (Western Summit)',
    metres: '4,164',
    range: 'Pennine Alps',
    location: 'Switzerland/Italy'
  },
  {
    mountain: 'Jungfrau',
    metres: '4,158',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Bishorn',
    metres: '4,153',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Helen',
    metres: '4,151',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'El Mela',
    metres: '4,150',
    range: 'Andes',
    location: 'Argentina'
  },
  {
    mountain: 'Mount Quincy Adams',
    metres: '4,150',
    range: 'Fairweather Range',
    location: 'Canada/US'
  },
  {
    mountain: 'Doublet Peak',
    metres: '4,145',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Turret Peak',
    metres: '4,145',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Parnassus (Colorado)',
    metres: '4,137',
    range: 'Front Range, Rocky Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Uludoruk (Reşko / Gelyaşin)',
    metres: '4,137',
    range: 'Cilo Daglari Hakkari',
    location: 'Turkey'
  },
  {
    mountain: 'Mount Sacagawea',
    metres: '4,136',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Dubois',
    metres: '4,135',
    range: 'White Mountains',
    location: 'California, US'
  },
  {
    mountain: 'Mount Muhabura',
    metres: '4,127',
    range: 'Virunga Mountains',
    location: 'Musanze, Rwanda'
  },
  {
    mountain: 'Kings Peak',
    metres: '4,125',
    range: 'Uinta Range',
    location: 'Utah, US'
  },
  {
    mountain: 'Aiguille Verte',
    metres: '4,122',
    range: 'Mont Blanc massif',
    location: 'France'
  },
  {
    mountain: 'Mount Bangeta',
    metres: '4,121',
    range: '',
    location: 'Papua New Guinea'
  },
  {
    mountain: 'Mount Dickerson',
    metres: '4,120',
    range: 'Queen Alexandra Range',
    location: 'Antarctica'
  },
  {
    mountain: 'Monte Tlaloc',
    metres: '4,120',
    range: 'Mexican Volcanic Belt',
    location: 'Mexico'
  },
  {
    mountain: 'Jackson Peak',
    metres: '4,120',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Cilo Dağı',
    metres: '4,116',
    range: 'Cilo Range',
    location: 'Eastern Taurus, Turkey'
  },
  {
    mountain: 'Mount Woodrow Wilson',
    metres: '4,115',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Aiguilles du Diable',
    metres: '4,114',
    range: 'Graian Alps',
    location: 'Mont Blanc massif, France'
  },
  {
    mountain: 'Bastion Peak',
    metres: '4,113',
    range: 'Wind River Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Aiguille Blanche de Peuterey',
    metres: '4,112',
    range: 'Mont Blanc massif',
    location: 'Italy'
  },
  {
    mountain: 'Mönch',
    metres: '4,107',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Febbas',
    metres: '4,105',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Barre des Écrins',
    metres: '4,102',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Mount Aragats',
    metres: '4,095',
    range: '',
    location: 'Armenia'
  },
  {
    mountain: 'Mount Cameroon',
    metres: '4,095',
    range: '',
    location: 'Cameroon'
  },
  {
    mountain: 'Mount Kinabalu',
    metres: '4,095',
    range: 'Sabah',
    location: 'Malaysia'
  },
  {
    mountain: 'Grizzly Peak D',
    metres: '4,093',
    range: 'Front Range, Rocky Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Pollux',
    metres: '4,092',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  { mountain: 'Kīlauea', metres: '4,091', range: '', location: 'Hawaii, US' },
  {
    mountain: 'Mount Wade',
    metres: '4,085',
    range: 'Prince Olav Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Nairamdal Peak',
    metres: '4,082',
    range: 'Altai Mountains',
    location: 'Mongolia'
  },
  {
    mountain: 'Mount Victoria',
    metres: '4,072',
    range: '',
    location: 'Papua New Guinea'
  },
  {
    mountain: 'Centennial Peak',
    metres: '4,070',
    range: 'Prince Olav Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Gran Paradiso',
    metres: '4,061',
    range: 'Graian Alps',
    location: 'Italy'
  },
  {
    mountain: 'Ober Gabelhorn',
    metres: '4,053',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Piz Bernina',
    metres: '4,049',
    range: 'Bernina Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Fiescherhorn',
    metres: '4,049',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Grünhorn',
    metres: '4,043',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Lauteraarhorn',
    metres: '4,042',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Aiguille du Géant',
    metres: '4,035',
    range: 'Graian Alps, Mont Blanc massif',
    location: 'France'
  },
  {
    mountain: 'Mount Silverthrone',
    metres: '4,030',
    range: 'Alaska Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Sniktau',
    metres: '4,034',
    range: 'Front Range, Rocky Mountains',
    location: 'Colorado, US'
  },
  { mountain: 'Azufral', metres: '4,030', range: '', location: 'Colombia' },
  {
    mountain: 'Allalinhorn',
    metres: '4,027',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Wind River Peak',
    metres: '4,021',
    range: 'Wind River Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Latsga',
    metres: '4,019',
    range: 'Caucasus Mountains',
    location: 'Svaneti, Georgia'
  },
  {
    mountain: 'Weissmies',
    metres: '4,017',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Waddington',
    metres: '4,016',
    range: 'British Columbia',
    location: 'Canada'
  },
  {
    mountain: 'Mount Marcus Baker',
    metres: '4,016',
    range: 'Chugach Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Dôme de neige des Écrins',
    metres: '4,015',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Conejos Peak',
    metres: '4,015',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Dent du Géant',
    metres: '4,013',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  {
    mountain: 'Red Slate Mountain',
    metres: '4,013',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Cloud Peak',
    metres: '4,013',
    range: 'Big Horn Mountains',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Wheeler Peak',
    metres: '4,012',
    range: 'Sangre de Cristo Mountains',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Lagginhorn',
    metres: '4,010',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Twilight Peak',
    metres: '4,010',
    range: 'San Juan Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Francs Peak',
    metres: '4,009',
    range: 'Absaroka Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Lahili',
    metres: '4,009',
    range: 'Svaneti Range',
    location: 'Svaneti, Georgia'
  },
  {
    mountain: 'Mount Walter',
    metres: '4,005',
    range: '',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Les Droites',
    metres: '4,000',
    range: 'Mont Blanc massif',
    location: 'France'
  },
  {
    mountain: 'Piz Zupò',
    metres: '3,995',
    range: 'Bernina Range',
    location: 'Switzerland'
  },
  {
    mountain: 'Truchas Peak',
    metres: '3,994',
    range: 'Sangre de Cristo Mountains',
    location: 'New Mexico'
  },
  {
    mountain: 'Fletschhorn',
    metres: '3,993',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Albert Edward',
    metres: '3,990',
    range: '',
    location: 'Papua New Guinea'
  },
  {
    mountain: 'La Meije',
    metres: '3,987',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Wheeler Peak',
    metres: '3,982',
    range: 'Snake Range',
    location: 'Nevada, US'
  },
  {
    mountain: 'Mount Dana',
    metres: '3,981',
    range: '',
    location: 'California, US'
  },
  { mountain: 'Acatenango', metres: '3,976', range: '', location: 'Guatemala' },
  {
    mountain: 'Piz Roseg',
    metres: '3,973',
    range: 'Bernina Range',
    location: 'Switzerland'
  },
  {
    mountain: 'Piz Scerscen',
    metres: '3,971',
    range: 'Bernina Range',
    location: 'Switzerland'
  },
  {
    mountain: 'Eiger',
    metres: '3,967',
    range: 'Bernese Oberland',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Tochal',
    metres: '3,964',
    range: 'Alborz',
    location: 'Iran'
  },
  {
    mountain: 'Black Tooth Mountain',
    metres: '3,964',
    range: 'Bighorn Mountains',
    location: 'Wyoming US'
  },
  {
    mountain: 'Grand Cornier',
    metres: '3,962',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Owen',
    metres: '3,957',
    range: 'Teton Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Ailefroide',
    metres: '3,954',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Mount Robson',
    metres: '3,954',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Descabezado Grande',
    metres: '3,953',
    range: '',
    location: 'Chile'
  },
  {
    mountain: 'Yu Shan (Mount Jade)',
    metres: '3,952',
    range: '',
    location: 'Taiwan and East Asia'
  },
  {
    mountain: 'Mont Pelvoux',
    metres: '3,946',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Mount Julian',
    metres: '3,940',
    range: '',
    location: 'Colorado, United States'
  },
  {
    mountain: 'Mount Khalatsa',
    metres: '3,938',
    range: 'Disputed',
    location: 'South Ossetia or Georgia'
  },
  { mountain: 'Ajusco', metres: '3,930', range: '', location: 'Mexico' },
  {
    mountain: 'Mount Erciyes',
    metres: '3,916',
    range: 'Central Anatolia',
    location: 'Turkey'
  },
  {
    mountain: 'Bomber Mountain',
    metres: '3,914',
    range: 'Bighorn Range',
    location: 'Wyoming, United States'
  },
  {
    mountain: 'Pic Sans Nom',
    metres: '3,919',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Jicarita Peak',
    metres: '3,912',
    range: 'Sangre de Cristo Mountains',
    location: 'New Mexico'
  },
  {
    mountain: 'Ortler',
    metres: '3,905',
    range: 'South Tyrol',
    location: 'Tyrol'
  },
  {
    mountain: 'Piz Palü',
    metres: '3,905',
    range: 'Bernina Range',
    location: 'Switzerland'
  },
  {
    mountain: 'Middle Teton',
    metres: '3,903',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: "Aiguille d'Argentière",
    metres: '3,902',
    range: 'Mont Blanc massif',
    location: 'France/Switzerland'
  },
  {
    mountain: 'Granite Peak',
    metres: '3,901',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Gibbs',
    metres: '3,893',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Crillon',
    metres: '3,879',
    range: 'Fairweather Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mont Blanc de Cheilon',
    metres: '3,870',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Aiguille du Goûter',
    metres: '3,863',
    range: 'Graian Alps, Mont Blanc massif',
    location: 'France'
  },
  { mountain: 'Borah Peak', metres: '3,861', range: '', location: 'Idaho, US' },
  {
    mountain: 'Grande Casse',
    metres: '3,855',
    range: 'Vanoise Massif',
    location: 'France'
  },
  {
    mountain: 'Humphreys Peak',
    metres: '3,852',
    range: '',
    location: 'Arizona, US'
  },
  {
    mountain: 'Santa Fe Baldy',
    metres: '3,847',
    range: 'Sangre de Cristo Mountains',
    location: 'New Mexico'
  },
  {
    mountain: 'Mount Baldwin',
    metres: '3,845',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Aiguille du Midi',
    metres: '3,842',
    range: 'Graian Alps, Mont Blanc Massif',
    location: 'France'
  },
  {
    mountain: 'Mount Moran',
    metres: '3,842',
    range: '',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Monte Viso',
    metres: '3,841',
    range: 'Italian Alps',
    location: 'Italy'
  },
  {
    mountain: 'Sauyr Zhotasy',
    metres: '3,840',
    range: '',
    location: 'Kazakhstan'
  },
  {
    mountain: 'Mount Saramati',
    metres: '3,826',
    range: 'Patkai Range',
    location: 'Nagaland, India'
  },
  {
    mountain: 'Nesthorn',
    metres: '3,822',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Cerro Chirripó',
    metres: '3,820',
    range: '',
    location: 'Costa Rica'
  },
  {
    mountain: 'Aiguille des Glaciers',
    metres: '3,816',
    range: 'Mont Blanc massif',
    location: 'France/Italy'
  },
  {
    mountain: 'South Teton',
    metres: '3,814',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'La Râteau',
    metres: '3,809',
    range: 'Dauphiné Alps',
    location: 'France'
  },
  {
    mountain: 'Mount Kerinci',
    metres: '3,800',
    range: '',
    location: 'Indonesia'
  },
  {
    mountain: 'Grossglockner',
    metres: '3,798',
    range: '',
    location: 'Austria'
  },
  {
    mountain: "Pigne d'Arolla",
    metres: '3,796',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Erebus',
    metres: '3,794',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mont Pourri',
    metres: '3,779',
    range: 'Vanoise Massif',
    location: 'France'
  },
  { mountain: 'Mount Fuji', metres: '3,776', range: '', location: 'Japan' },
  { mountain: 'Wildspitze', metres: '3,774', range: '', location: 'Austria' },
  {
    mountain: 'Greenhorn Mountain',
    metres: '3,763',
    range: 'Wet Mountains',
    location: 'Colorado, US'
  },
  {
    mountain: 'Volcán de Fuego',
    metres: '3,763',
    range: '',
    location: 'Guatemala'
  },
  {
    mountain: 'Mount Villard',
    metres: '3,760',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Teewinot Mountain',
    metres: '3,757',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Aiguille du Dru',
    metres: '3,754',
    range: 'Graian Alps',
    location: 'France'
  },
  {
    mountain: 'Aoraki / Mount Cook',
    metres: '3,754',
    range: '',
    location: 'New Zealand'
  },
  {
    mountain: 'Pointe de Charbonnel',
    metres: '3,752',
    range: 'Graian Alps',
    location: 'France'
  },
  {
    mountain: 'Piz Morteratsch',
    metres: '3,751',
    range: 'Bernina Range',
    location: 'Switzerland'
  },
  {
    mountain: 'Sawtooth Mountain',
    metres: '3,750',
    range: 'Front Range',
    location: 'Colorado, US'
  },
  {
    mountain: 'Mount Davis',
    metres: '3,750',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Morrison',
    metres: '3,750',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Taibai',
    metres: '3,750',
    range: 'Qin Mountains',
    location: 'China'
  },
  {
    mountain: 'Aiguille de la Grande Sassière',
    metres: '3,747',
    range: 'Graian Alps',
    location: 'France'
  },
  {
    mountain: 'Lanín',
    metres: '3,747',
    range: 'Andes',
    location: 'Chile/Argentina'
  },
  {
    mountain: 'Mount Adams',
    metres: '3,743',
    range: 'Cascade Range',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Columbia',
    metres: '3,747',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Adams',
    metres: '3,743',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Teepe Pillar',
    metres: '3,739',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Weißkugel',
    metres: '3,739',
    range: 'Swiss Alps',
    location: 'Austria/Italy'
  },
  {
    mountain: 'Minarets',
    metres: '3,735',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Huntington',
    metres: '3,731',
    range: 'Alaska Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Bedori Peak',
    metres: '3,727',
    range: 'Forward Kahuta',
    location: 'Azad Kashmir, Pakistan'
  },
  {
    mountain: 'Mount Rinjani',
    metres: '3,726',
    range: '',
    location: 'Indonesia'
  },
  {
    mountain: 'Cerro del Potosí',
    metres: '3,721',
    range: '',
    location: 'Mexico'
  },
  {
    mountain: 'Diamond Peak',
    metres: '3,719',
    range: '',
    location: 'Idaho, US'
  },
  { mountain: 'Teide', metres: '3,718', range: '', location: 'Canary Islands' },
  {
    mountain: 'Asperity Mountain',
    metres: '3,716',
    range: 'Waddington Range',
    location: 'Canada'
  },
  {
    mountain: 'Cerro de la Viga',
    metres: '3,712',
    range: '',
    location: 'Mexico'
  },
  { mountain: 'Delano Peak', metres: '3,711', range: '', location: 'Utah, US' },
  {
    mountain: 'Sahand',
    metres: '3,707',
    range: 'East Azerbaijan',
    location: 'Iran'
  },
  {
    mountain: 'Monte San Lorenzo',
    metres: '3,706',
    range: 'Patagonia',
    location: 'Argentina-Chile'
  },
  {
    mountain: 'Wetterhorn',
    metres: '3,701',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Valhalla',
    metres: '3,699',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Dent Parrachée',
    metres: '3,697',
    range: 'Vanoise massif',
    location: 'France'
  },
  { mountain: 'Gunnbjørn', metres: '3,694', range: '', location: 'Greenland' },
  {
    mountain: 'North Twin Peak',
    metres: '3,684',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Monte Disgrazia',
    metres: '3,678',
    range: 'Bregaglia',
    location: 'Switzerland'
  },
  { mountain: 'Semeru', metres: '3,676', range: 'Java', location: 'Indonesia' },
  {
    mountain: 'Les Bans',
    metres: '3,669',
    range: 'Ecrins',
    location: 'France'
  },
  {
    mountain: 'Cloudveil Dome',
    metres: '3,666',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Thor Peak',
    metres: '3,666',
    range: 'Teton Range',
    location: 'Wyoming, United States'
  },
  {
    mountain: 'Medicine Bow Peak',
    metres: '3,661',
    range: '',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Hyndman Peak',
    metres: '3,660',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Clemenceau',
    metres: '3,658',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Frakes',
    metres: '3,654',
    range: 'Crary Mountains',
    location: 'Antarctica'
  },
  {
    mountain: 'Grande Motte',
    metres: '3,653',
    range: 'Vanoise Massif',
    location: 'France'
  },
  {
    mountain: 'Sierra Blanca',
    metres: '3,652',
    range: 'Sacramento Mountains',
    location: 'New Mexico'
  },
  {
    mountain: 'Pointe de la Fournache',
    metres: '3,642',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Buck Mountain',
    metres: '3,639',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  { mountain: 'Mount Nebo', metres: '3,636', range: '', location: 'Utah, US' },
  {
    mountain: 'Mount Charleston',
    metres: '3,632',
    range: '',
    location: 'Nevada, US'
  },
  {
    mountain: 'Hintere Schwärze',
    metres: '3,628',
    range: 'Ötztal Alps',
    location: 'Austria/Italy'
  },
  {
    mountain: 'Nez Perce Peak',
    metres: '3,627',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Alberta',
    metres: '3,619',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Assiniboine',
    metres: '3,618',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Tödi',
    metres: '3,614',
    range: 'Swiss Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Forbes',
    metres: '3,612',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Lautaro',
    metres: '3,607',
    range: 'Patagonia',
    location: 'Chile'
  },
  {
    mountain: 'Dôme de la Sache',
    metres: '3,601',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Castle Peak',
    metres: '3,601',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: "Dôme de l'Arpont",
    metres: '3,601',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Dôme de Chasseforêt',
    metres: '3,586',
    range: 'Vanoise massif',
    location: 'France'
  },
  { mountain: 'Makra Peak', metres: '3,586', range: '', location: 'Pakistan' },
  { mountain: 'Sierra Velluda', metres: '3,585', range: '', location: 'Chile' },
  {
    mountain: 'Grand Roc Noir',
    metres: '3,582',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Timpanogos',
    metres: '3,582',
    range: '',
    location: 'Utah, United States'
  },
  {
    mountain: 'Mount Alvand',
    metres: '3,580',
    range: 'Hamedan',
    location: 'Iran'
  },
  {
    mountain: 'Dôme des Nants',
    metres: '3,570',
    range: 'Vanoise massif',
    location: 'France'
  },
  { mountain: 'Ryan Peak', metres: '3,570', range: '', location: 'Idaho, US' },
  {
    mountain: 'South Twin Peak',
    metres: '3,566',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  { mountain: 'Olan', metres: '3,564', range: 'Ecrins', location: 'France' },
  {
    mountain: 'Aiguille de Péclet',
    metres: '3,561',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Presanella',
    metres: '3,558',
    range: 'Adamello-Presanella',
    location: 'Italy'
  },
  {
    mountain: 'Mount Steere',
    metres: '3,558',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Monarch Mountain',
    metres: '3,555',
    range: 'Pacific Ranges',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Monte Leone',
    metres: '3,552',
    range: 'Lepontine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mont Turia',
    metres: '3,550',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Temple',
    metres: '3,543',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Disappointment Peak',
    metres: '3,541',
    range: 'Teton Range',
    location: 'Wyoming US'
  },
  {
    mountain: 'Mount Woodring',
    metres: '3,533',
    range: 'Teton Range',
    location: ' Wyoming US'
  },
  {
    mountain: 'Aiguille de Polset',
    metres: '3,531',
    range: 'Vanoise Massif',
    location: 'France'
  },
  {
    mountain: 'Snow Dome',
    metres: '3,520',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: "Aiguilles d'Arves",
    metres: '3,515',
    range: 'Arve Massif',
    location: 'France'
  },
  {
    mountain: 'Mount Ellen (Utah)',
    metres: '3,513',
    range: 'Henry Mountains',
    location: 'Utah, US'
  },
  {
    mountain: 'Mont de Gébroulaz',
    metres: '3,511',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount San Gorgonio',
    metres: '3,505',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Kitchener',
    metres: '3,505',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Zuckerhütl',
    metres: '3,505',
    range: 'Stubai Alps',
    location: 'Austria'
  },
  {
    mountain: 'Mount Wister',
    metres: '3,502',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Saskatchewan',
    metres: '3,500',
    range: 'Yukon',
    location: 'Canada'
  },
  {
    mountain: 'Mount Tasman',
    metres: '3,497',
    range: 'Southern Alps',
    location: 'New Zealand'
  },
  {
    mountain: 'Mount Hungabee',
    metres: '3,492',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Athabasca',
    metres: '3,491',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Tronador',
    metres: '3,491',
    range: '',
    location: 'Chile/Argentina'
  },
  {
    mountain: 'Mount Saint John',
    metres: '3,484',
    range: 'Teton Range',
    location: 'Wyoming US'
  },
  {
    mountain: 'Thabana Ntlenyana',
    metres: '3,482',
    range: 'Drakensberg',
    location: 'Africa'
  },
  {
    mountain: 'Pointes du Châtelard',
    metres: '3,479',
    range: 'Vanoise massif',
    location: 'France'
  },
  { mountain: 'Mulhacén', metres: '3,479', range: '', location: 'Spain' },
  {
    mountain: 'Mount Berlin',
    metres: '3,478',
    range: 'Flood Range',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Pennell',
    metres: '3,478',
    range: 'Henry Mountains, Utah',
    location: 'US'
  },
  {
    mountain: 'Volcán Barú',
    metres: '3,475',
    range: 'Chiriquí',
    location: 'Panama'
  },
  {
    mountain: 'Koh-i-Takatu Sraghurgai',
    metres: '3,472',
    range: 'Quetta Sraghurgai, Suleman Range',
    location: 'Pakistan'
  },
  {
    mountain: 'Roc des Saints Pères',
    metres: '3,470',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Brazeau',
    metres: '3,470',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Nyiragongo',
    metres: '3,470',
    range: 'Virunga Mountains DRC',
    location: 'Congo'
  },
  {
    mountain: 'Ruby Dome',
    metres: '3,470',
    range: 'Ruby Mountains',
    location: 'Nevada'
  },
  {
    mountain: 'Roche de la Muzelle',
    metres: '3,465',
    range: 'Dauphine Alps',
    location: 'France'
  },
  {
    mountain: 'Mount Victoria',
    metres: '3,464',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Abajo Peak',
    metres: '3,463',
    range: 'Abajo Mountains',
    location: 'Utah'
  },
  {
    mountain: 'Eagle Peak',
    metres: '3,462',
    range: 'Absaroka Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Doane Peak',
    metres: '3,461',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Ranger Peak',
    metres: '3,461',
    range: 'Teton range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Takahe',
    metres: '3,460',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Cerro de la Muerte',
    metres: '3,451',
    range: '',
    location: 'Costa Rica'
  },
  {
    mountain: 'Furgghorn',
    metres: '3,451',
    range: 'Pennine Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mafadi',
    metres: '3,450',
    range: 'Drakensberg',
    location: 'South Africa'
  },
  {
    mountain: 'Mount Andromeda',
    metres: '3,450',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Boulder Mountain',
    metres: '3,449',
    range: '',
    location: 'Utah, US'
  },
  {
    mountain: 'Mount Joffre',
    metres: '3,449',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Hilgard Peak',
    metres: '3,449',
    range: 'Madison Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Static Peak',
    metres: '3,445',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Thousand Lake Mountain',
    metres: '3,444',
    range: '',
    location: 'Utah, US'
  },
  {
    mountain: 'Pointe de la Sana',
    metres: '3,436',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Eagles Rest Peak',
    metres: '3,431',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Irazú Volcano',
    metres: '3,431',
    range: '',
    location: 'Costa Rica'
  },
  {
    mountain: 'Mount Hood',
    metres: '3,429',
    range: 'Cascade Range',
    location: 'Oregon, US'
  },
  {
    mountain: 'Verpeilspitze',
    metres: '3,425',
    range: '',
    location: 'Austria'
  },
  {
    mountain: 'Deltaform Mountain',
    metres: '3,424',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Lefroy',
    metres: '3,423',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: "Pointe de l'Échelle",
    metres: '3,422',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Pointe du Bouchet',
    metres: '3,420',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Fitzgerald',
    metres: '3,418',
    range: 'Ruby Mountains',
    location: 'Nevada, US'
  },
  {
    mountain: 'Bellecôte',
    metres: '3,417',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Crazy Peak',
    metres: '3,417',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Emi Koussi',
    metres: '3,415',
    range: 'Tibesti Mountains',
    location: 'Chad'
  },
  {
    mountain: 'Piz Linard',
    metres: '3,410',
    range: '',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Woolley',
    metres: '3,405',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  { mountain: 'Aneto', metres: '3,404', range: 'Pyrenees', location: 'Spain' },
  {
    mountain: 'Lone Mountain',
    metres: '3,404',
    range: 'Madison Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Fluchthorn',
    metres: '3,399',
    range: 'Silvretta',
    location: 'Austria-Switzerland'
  },
  {
    mountain: 'Grand Bec',
    metres: '3,398',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Rockchuck Peak',
    metres: '3,397',
    range: 'Teton Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Pico Veleta',
    metres: '3,396',
    range: 'Sierra Nevada',
    location: 'Spain'
  },
  {
    mountain: 'Mount Hector',
    metres: '3,394',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Piz Platta',
    metres: '3,392',
    range: 'Swiss Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Telescope Peak',
    metres: '3,392',
    range: 'Death Valley',
    location: 'US'
  },
  {
    mountain: 'Champagne Castle',
    metres: '3,377',
    range: 'Drakensberg',
    location: 'South Africa'
  },
  {
    mountain: 'Pic Uzu',
    metres: '3,376',
    range: 'Tibesti',
    location: 'Libya-Chad'
  },
  {
    mountain: 'Cerro Chaltén',
    metres: '3,375',
    range: 'Patagonia',
    location: 'Argentina-Chile'
  },
  { mountain: 'Mount Spurr', metres: '3,374', range: '', location: 'Alaska' },
  {
    mountain: 'Pointe du Vallonnet',
    metres: '3,372',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mammoth Mountain',
    metres: '3,371',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Fründenhorn',
    metres: '3,369',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Pointe Renod',
    metres: '3,368',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Traverse Peak',
    metres: '3,368',
    range: 'Teton Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Mount Edith Cavell',
    metres: '3,363',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Dôme des Sonnailles',
    metres: '3,361',
    range: 'Vanoise massif',
    location: 'France'
  },
  { mountain: 'Valvelspitze', metres: '3,360', range: '', location: 'Austria' },
  {
    mountain: 'Mount Etna',
    metres: '3,357',
    range: 'Metropolitan City of Catania',
    location: 'Italy'
  },
  {
    mountain: 'Mount Agepsta',
    metres: '3,357',
    range: 'Gagra Range',
    location: 'Abkhazia, Georgia'
  },
  {
    mountain: 'Mount Munday',
    metres: '3,356',
    range: 'Pacific Ranges',
    location: 'B.C, Canada'
  },
  {
    mountain: 'Pointe de Claret',
    metres: '3,355',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Monte Perdido',
    metres: '3,355',
    range: 'Pyrenees',
    location: 'Spain'
  },
  {
    mountain: 'Electric Peak',
    metres: '3,343',
    range: 'Gallatin Range',
    location: 'Montana US'
  },
  {
    mountain: 'Marmolada',
    metres: '3,343',
    range: 'Dolomites',
    location: 'Italy'
  },
  {
    mountain: 'Mount Saskatchewan',
    metres: '3,342',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Cerro Fábrega',
    metres: '3,335',
    range: 'Bocas Del Toro',
    location: 'Panama'
  },
  {
    mountain: 'Ward Mountain',
    metres: '3,333',
    range: '',
    location: 'Nevada, US'
  },
  {
    mountain: 'Pointe de Méan Martin',
    metres: '3,330',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Dôme de Polset',
    metres: '3,326',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Cathedral Peak',
    metres: '3,326',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Raynolds Peak',
    metres: '3,325',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Rolling Thunder Mountain',
    metres: '3,324',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Hampton',
    metres: '3,323',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Dôme des Pichères',
    metres: '3,319',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Grand Roc',
    metres: '3,316',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: "Giant's Castle",
    metres: '3,315',
    range: 'Drakensberg',
    location: 'South Africa'
  },
  {
    mountain: 'Sunwapta Peak',
    metres: '3,315',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Piz Buin',
    metres: '3,312',
    range: 'Silvretta',
    location: 'Austria-Switzerland'
  },
  {
    mountain: 'Mount Ball',
    metres: '3,311',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Piz Badile',
    metres: '3,308',
    range: 'Bregaglia',
    location: 'Switzerland'
  },
  {
    mountain: 'Ağ Dağ',
    metres: '3,306',
    range: 'Bozgush mountain range',
    location: 'East Azerbaijan, Iran'
  },
  {
    mountain: 'San Jacinto Peak',
    metres: '3,302',
    range: '',
    location: 'California, US'
  },
  { mountain: 'Didi Abuli', metres: '3,301', range: '', location: 'Georgia' },
  {
    mountain: 'Silberhorn',
    metres: '3,300',
    range: 'Southern Alps',
    location: 'New Zealand'
  },
  {
    mountain: 'Bivouac Peak',
    metres: '3,299',
    range: 'Teton Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Mount Wilbur',
    metres: '3,298',
    range: 'Fairweather Range',
    location: 'Alaska, US'
  },
  {
    mountain: 'Monte Argentera',
    metres: '3,297',
    range: 'Maritime Alps',
    location: 'Italy'
  },
  {
    mountain: 'Mount Rose',
    metres: '3,287',
    range: 'Carson Range',
    location: 'Nevada'
  },
  {
    mountain: 'Mount Samsari',
    metres: '3,285',
    range: 'Abul-Samsari Range',
    location: 'Georgia'
  },
  {
    mountain: 'Roche Chevrière',
    metres: '3,281',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Habicht',
    metres: '3,277',
    range: 'Stubai Alps',
    location: 'Austria'
  },
  {
    mountain: 'Thompson Peak',
    metres: '3,277',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Chephren',
    metres: '3,266',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Pointe de Thorens',
    metres: '3,266',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Cramer',
    metres: '3,266',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Toussidé',
    metres: '3,265',
    range: 'Tibesti Mountains',
    location: 'Chad-Libya'
  },
  {
    mountain: 'Antelao',
    metres: '3,264',
    range: 'Dolomites',
    location: 'Italy'
  },
  {
    mountain: 'Mont Pelve',
    metres: '3,261',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Stanley Baldwin',
    metres: '3,256',
    range: 'Columbia Mountains',
    location: 'B.C., Canada'
  },
  {
    mountain: 'Sandia Crest',
    metres: '3,255',
    range: '',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Épaule du Bouchet',
    metres: '3,250',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Smythe',
    metres: '3,246',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Wildstrubel',
    metres: '3,243',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Williams Peak',
    metres: '3,242',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Hope',
    metres: '3,239',
    range: '',
    location: 'British Antarctic Territory'
  },
  {
    mountain: 'Titlis',
    metres: '3,238',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Owl Peak',
    metres: '3,235',
    range: 'Teton Range',
    location: 'Wyoming US'
  },
  {
    mountain: 'Mount Mackenzie King',
    metres: '3,234',
    range: 'Cariboo Mountains',
    location: 'B.C., Canada'
  },
  {
    mountain: 'Pointe des Buffettes',
    metres: '3,233',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Lisenser Spitze',
    metres: '3,230',
    range: 'Stubai Alps',
    location: 'Austria'
  },
  {
    mountain: 'Mount Terror',
    metres: '3,230',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Aiguille Rouge',
    metres: '3,227',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Monte Civetta',
    metres: '3,220',
    range: 'Dolomites',
    location: 'Italy'
  },
  {
    mountain: 'Symmetry Spire',
    metres: '3,219',
    range: 'Teton Range',
    location: 'Wyoming'
  },
  {
    mountain: 'Glacier Peak',
    metres: '3,213',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Pointe du Dard',
    metres: '3,212',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Binalud',
    metres: '3,211',
    range: '',
    location: 'Alborz, Iran'
  },
  {
    mountain: 'Pointe de la Réchasse',
    metres: '3,206',
    range: 'Vanoise massif',
    location: 'France'
  },
  {
    mountain: 'Mount Jefferson',
    metres: '3,204',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Jefferson',
    metres: '3,199',
    range: 'Cascade Range',
    location: 'Oregon, US'
  },
  {
    mountain: 'Dreiländerspitze',
    metres: '3,197',
    range: 'Silvretta',
    location: 'Austria-Switzerland'
  },
  {
    mountain: 'Mount Kita',
    metres: '3,193',
    range: 'Akaishi Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Mount Hotaka',
    metres: '3,190',
    range: 'Hida Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Mount Cleveland',
    metres: '3,190',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Ainodake',
    metres: '3,189',
    range: 'Akaishi Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Lassen Peak',
    metres: '3,189',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Galatea',
    metres: '3,185',
    range: 'Kananaskis Range',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Rendezvous Mountain',
    metres: '3,185',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Saviers Peak',
    metres: '3,182',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Yari',
    metres: '3,180',
    range: 'Hida Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Kendrick Peak',
    metres: '3,178',
    range: 'San Francisco Peaks',
    location: 'US'
  },
  {
    mountain: 'Kendrick Peak',
    metres: '3,178',
    range: 'San Francisco Peaks',
    location: 'US'
  },
  { mountain: 'Parícutin', metres: '3,170', range: '', location: 'Mexico' },
  {
    mountain: 'Mount Aylmer',
    metres: '3,162',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'South Sister',
    metres: '3,157',
    range: 'Cascade Range',
    location: 'Oregon, US'
  },
  {
    mountain: 'Stanley Peak',
    metres: '3,155',
    range: 'Ball Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Jøkulkyrkja',
    metres: '3,148',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: "Pica d'Estats",
    metres: '3,143',
    range: 'Pyrenees',
    location: 'Spanish-French border'
  },
  {
    mountain: 'Phan Xi Păng',
    metres: '3,143',
    range: '',
    location: 'Sa pa, Vietnam'
  },
  {
    mountain: 'Mount Arakawa',
    metres: '3,141',
    range: 'Akaishi Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Gray Peak',
    metres: '3,140',
    range: 'Yellowstone National Park',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Heyburn',
    metres: '3,139',
    range: '',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Washburn',
    metres: '3,122',
    range: 'Yellowstone National Park',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Collie',
    metres: '3,116',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Siple',
    metres: '3,110',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Pirchinassi',
    metres: '3,110',
    range: '',
    location: 'Muzaffarabad Azad Kashmir'
  },
  {
    mountain: 'Mount Redoubt',
    metres: '3,108',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Babel',
    metres: '3,101',
    range: 'Bow Range',
    location: 'Alberta, Canada'
  },
  { mountain: 'Mount Emei', metres: '3,099', range: '', location: 'China' },
  {
    mountain: 'Pico Duarte',
    metres: '3,098',
    range: '',
    location: 'Dominican Republic'
  },
  {
    mountain: 'Trapper Peak',
    metres: '3,096',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Storm Mountain',
    metres: '3,095',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Blackmore',
    metres: '3,094',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Stimson',
    metres: '3,091',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mol Len',
    metres: '3,088',
    range: 'Patkai Range',
    location: 'Nagaland-India/Burma'
  },
  {
    mountain: 'Qurnat as Sawdā’',
    metres: '3,088',
    range: '',
    location: 'Lebanon'
  },
  {
    mountain: 'Mount Richardson',
    metres: '3,086',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Kintla Peak',
    metres: '3,079',
    range: 'Livingston Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Moulton',
    metres: '3,078',
    range: '',
    location: 'Antarctica'
  },
  { mountain: 'Smoky Dome', metres: '3,077', range: '', location: 'Idaho, US' },
  { mountain: 'Jebel akthar', metres: '3,075', range: '', location: 'Oman' },
  {
    mountain: 'North Sister',
    metres: '3,074',
    range: 'Cascade Range',
    location: 'Oregon, US'
  },
  {
    mountain: 'Piton des Neiges',
    metres: '3,069',
    range: '',
    location: 'Réunion, France'
  },
  {
    mountain: 'Mount San Antonio',
    metres: '3,068',
    range: 'San Gabriel Mountains',
    location: 'California, US'
  },
  { mountain: 'Mount Ontake', metres: '3,067', range: '', location: 'Japan' },
  {
    mountain: 'Mount Jackson',
    metres: '3,064',
    range: 'Lewis Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Middle Sister',
    metres: '3,062',
    range: 'Cascade Range',
    location: 'Oregon, US'
  },
  {
    mountain: 'Mount Wutai',
    metres: '3,061',
    range: '',
    location: 'Shanxi, China'
  },
  {
    mountain: 'Mount Nyamuragira',
    metres: '3,058',
    range: '',
    location: 'Democratic Republic of the Congo'
  },
  { mountain: 'Haleakala', metres: '3,057', range: '', location: 'Hawaii, US' },
  {
    mountain: 'Crowfoot Mountain',
    metres: '3,055',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Chester',
    metres: '3,054',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Siyeh',
    metres: '3,052',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Merritt',
    metres: '3,049',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Cerro de Coxóm',
    metres: '3,045',
    range: '',
    location: 'Guatemala'
  },
  {
    mountain: 'Parseierspitze',
    metres: '3,036',
    range: '',
    location: 'Austria'
  },
  {
    mountain: 'Mount Senjō',
    metres: '3,033',
    range: 'Akaishi Mountains',
    location: 'japan'
  },
  {
    mountain: 'Mount Aspiring/Tititea',
    metres: '3,033',
    range: '',
    location: 'New Zealand'
  },
  {
    mountain: 'Kinnerly Peak',
    metres: '3,031',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Agung',
    metres: '3,031',
    range: '',
    location: 'Bali, Indonesia'
  },
  {
    mountain: 'Mount McArthur',
    metres: '3,021',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Norikura',
    metres: '3,026',
    range: 'Hida Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Tsiteli Khati',
    metres: '3,026',
    range: 'Kharuli Range',
    location: 'Georgia'
  },
  {
    mountain: 'Mount Tate',
    metres: '3,015',
    range: 'Hida Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Mount Japvo',
    metres: '3,014',
    range: 'Barail Range',
    location: 'Nagaland, India'
  },
  { mountain: 'Mount Tahat', metres: '3,003', range: '', location: 'Algeria' },
  {
    mountain: 'Sapitwa',
    metres: '3,002',
    range: 'Mulanje Massif',
    location: 'Malawi'
  },
  {
    mountain: 'The Fortress',
    metres: '3,000',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Tre Cime di Lavaredo',
    metres: '2,999',
    range: 'Province of Belluno',
    location: 'Italy'
  },
  {
    mountain: 'Pizzo Centrale',
    metres: '2,999',
    range: '',
    location: 'Switzerland'
  },
  {
    mountain: 'Cascade Mountain',
    metres: '2,998',
    range: 'Vermillion Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Copahue',
    metres: '2,997',
    range: 'Andes',
    location: 'Argentina/Chile'
  },
  {
    mountain: 'Mount Girouard',
    metres: '2,985',
    range: 'Fairholme Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Pico da Neblina',
    metres: '2,994',
    range: 'Serra do Imeri',
    location: 'Amazonas, Brazil'
  },
  {
    mountain: 'Mount Whyte',
    metres: '2,983',
    range: 'Bow Range, Canadian Rockies',
    location: 'Canada'
  },
  { mountain: 'Miranjani', metres: '2,980', range: '', location: 'Pakistan' },
  {
    mountain: 'Forellen Peak',
    metres: '2,979',
    range: 'Teton Range',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Andrus',
    metres: '2,978',
    range: 'Ames Range',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Niblock',
    metres: '2,976',
    range: 'Bow Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Pico 31 de Março',
    metres: '2,973',
    range: 'Serra do Imeri',
    location: 'Amazonas, Brazil'
  },
  {
    mountain: 'Keele Peak',
    metres: '2,972',
    range: 'Mackenzie Mountains',
    location: 'Yukon, Canada'
  },
  {
    mountain: 'Mount Odin',
    metres: '2,970',
    range: 'Monashee Mountains',
    location: 'Canada'
  },
  {
    mountain: 'Schilthorn',
    metres: '2,970',
    range: 'Bernese Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Tallac',
    metres: '2,968',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Inglismaldie',
    metres: '2,964',
    range: 'Fairholme Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Ramelau',
    metres: '2,963',
    range: '',
    location: 'East Timor'
  },
  { mountain: 'Zugspitze', metres: '2,962', range: '', location: 'Germany' },
  {
    mountain: 'Slide Mountain (Nevada)',
    metres: '2,957',
    range: 'Carson Range',
    location: 'Nevada'
  },
  {
    mountain: 'Little Alberta',
    metres: '2,956',
    range: 'Sir Winston Churchill Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Arfak',
    metres: '2,955',
    range: 'Arfak Mountains',
    location: 'West Papua, Indonesia'
  },
  {
    mountain: 'Iron Mountain',
    metres: '2,955',
    range: 'Soldier Mountains',
    location: 'Idaho, US'
  },
  { mountain: 'Apo', metres: '2,954', range: '', location: 'Philippines' },
  {
    mountain: 'Fossil Mountain',
    metres: '2,946',
    range: 'Banff NP',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Coma Pedrosa',
    metres: '2,942',
    range: 'Pyrenees',
    location: 'Andorra'
  },
  {
    mountain: 'Going to the Sun Mountain',
    metres: '2,939',
    range: 'Lewis Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Fishers Peak',
    metres: '2,936',
    range: 'Raton Mesas',
    location: 'Colorado-New Mexico, US'
  },
  {
    mountain: 'Pilot Mountain',
    metres: '2,935',
    range: 'Massive Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Uri Rotstock',
    metres: '2,928',
    range: '',
    location: 'Switzerland'
  },
  {
    mountain: 'Musala',
    metres: '2,925',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Pulag',
    metres: '2,926',
    range: '',
    location: 'Philippines'
  },
  {
    mountain: 'Mount Olympus',
    metres: '2,917',
    range: '',
    location: 'Thessaly/Macedonia, Greece'
  },
  {
    mountain: 'Vihren',
    metres: '2,914',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Gould',
    metres: '2,912',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Corno Grande',
    metres: '2,912',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Mount Blakiston',
    metres: '2,910',
    range: 'Flathead Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Puigmal',
    metres: '2,909',
    range: 'Pyrenees',
    location: 'Spanish-French border'
  },
  {
    mountain: 'Kutelo',
    metres: '2,908',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Sas dla Crusc',
    metres: '2,907',
    range: 'Dolomites',
    location: 'Italy'
  },
  {
    mountain: 'Chutine Peak',
    metres: '2,903',
    range: 'Coast Range',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Malka Musala',
    metres: '2,902',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Rising Wolf Mountain',
    metres: '2,900',
    range: 'Lewis Range',
    location: 'Montana, US'
  },
  {
    mountain: 'Bonanza Peak',
    metres: '2,899',
    range: 'Cascade Range',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount McLoughlin',
    metres: '2,894',
    range: 'Mount McLoughlin',
    location: 'Oregon, US'
  },
  {
    mountain: 'Pic de Sanfonts',
    metres: '2,894',
    range: 'Pyrenees',
    location: 'Andorra'
  },
  {
    mountain: 'Pico da Bandeira',
    metres: '2,892',
    range: 'Serra do Caparaó',
    location: 'Minas Gerais/Espírito Santo, Brazil'
  },
  {
    mountain: 'Collarada',
    metres: '2,886',
    range: 'Pyrenees',
    location: 'Spain'
  },
  {
    mountain: 'Banski Suhodol',
    metres: '2,884',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Boardman Peak',
    metres: '2,882',
    range: 'Soldier Mountains',
    location: 'Idaho, US'
  },
  {
    mountain: 'Mount Wrightson',
    metres: '2,882',
    range: 'Santa Rita Mountains',
    location: 'Arizona, US'
  },
  {
    mountain: 'Lembert Dome',
    metres: '2,880',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Sacajawea Peak',
    metres: '2,876',
    range: 'Wallowa Mountains',
    location: 'Oregon, US'
  },
  {
    mountain: 'Mount Stuart',
    metres: '2,869',
    range: 'Cascades',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Fisht',
    metres: '2,867',
    range: 'Adygea',
    location: 'Russia'
  },
  {
    mountain: 'Mount Silverthrone',
    metres: '2,865',
    range: 'British Columbia',
    location: 'Canada'
  },
  {
    mountain: 'Mount Baden-Powell',
    metres: '2,864.75',
    range: 'San Gabriel Mountain',
    location: 'California, US'
  },
  { mountain: 'Triglav', metres: '2,864', range: '', location: 'Slovenia' },
  {
    mountain: 'Mount Saint Nicholas',
    metres: '2,858',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Irechek',
    metres: '2,852',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Polezhan',
    metres: '2,851',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Tabayoc',
    metres: '2,842',
    range: 'Cordillera Range',
    location: 'Benguet, Philippines'
  },
  {
    mountain: 'Mount Wilbur',
    metres: '2,841',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Pico do Fogo',
    metres: '2,829',
    range: '',
    location: 'Cape Verde'
  },
  {
    mountain: 'Kamenitsa',
    metres: '2,822',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Bayuvi dupki',
    metres: '2,820',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Fernow',
    metres: '2,819',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Hermon',
    metres: '2,814',
    range: '',
    location: 'Syria and Lebanon'
  },
  {
    mountain: 'Mount Deakin',
    metres: '2,810',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Cory',
    metres: '2,802',
    range: '',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Mount Thielsen',
    metres: '2,799',
    range: 'Cascades',
    location: 'Oregon, US'
  },
  {
    mountain: 'Pedra da Mina',
    metres: '2,798',
    range: 'Serra da Mantiqueira',
    location: 'Minas Gerais/São Paulo, Brazil'
  },
  {
    mountain: 'Mount Ruapehu',
    metres: '2,797',
    range: 'Tongariro National Park',
    location: 'North Island, New Zealand'
  },
  {
    mountain: 'Copper Mountain',
    metres: '2,795',
    range: '',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Maiella',
    metres: '2,793',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Pico das Agulhas Negras',
    metres: '2,792',
    range: 'Serra do Itatiaia',
    location: 'Minas Gerais/Rio de Janeiro, Brazil'
  },
  {
    mountain: 'Deno',
    metres: '2,790',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Hayes Volcano',
    metres: '2,788',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Edziza',
    metres: '2,787',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Canigou',
    metres: '2,784',
    range: 'Pyrenees',
    location: 'France'
  },
  {
    mountain: 'Mount Weaver',
    metres: '2,780',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Buckner Mountain',
    metres: '2,778',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Longonot',
    metres: '2,776',
    range: 'Great Rift Valley',
    location: 'Kenya'
  },
  {
    mountain: 'Seven Fingered Jack',
    metres: '2,774',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Nirvana',
    metres: '2,773',
    range: 'Northwest Territories',
    location: 'Canada'
  },
  {
    mountain: 'Pico do Cristal',
    metres: '2,770',
    range: 'Serra do Caparaó, Minas Gerais',
    location: 'Brazil'
  },
  {
    mountain: 'Chief Mountain',
    metres: '2,768',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Ovcharets',
    metres: '2,768',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Lincoln Peak',
    metres: '2,768',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Castle Mountain',
    metres: '2,766',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Pyramid Mountain',
    metres: '2,766',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Pico Naiguata (Avila)',
    metres: '2,765',
    range: 'Caracas',
    location: 'Venezuela'
  },
  {
    mountain: 'Mount Bachelor',
    metres: '2,764',
    range: 'Formerly Bachelor Butte, Cascade Mountains',
    location: 'Oregon, US'
  },
  {
    mountain: 'Mount Korab',
    metres: '2,764',
    range: '',
    location: 'North Macedonia & Albania'
  },
  {
    mountain: 'Yalovarnika',
    metres: '2,763',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Gazey',
    metres: '2,761',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Sir James MacBrien',
    metres: '2,759',
    range: '',
    location: 'Canada'
  },
  {
    mountain: 'Kaymakchal',
    metres: '2,753',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Olympus',
    metres: '2,751',
    range: '',
    location: 'Utah, US'
  },
  {
    mountain: 'Todorka',
    metres: '2,746',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mawson Peak',
    metres: '2,745',
    range: '',
    location: 'Australian Antarctic Territory'
  },
  {
    mountain: 'Fairview Mountain',
    metres: '2,744',
    range: 'Bow Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Baekdu',
    metres: '2,744',
    range: '',
    location: 'North Korea/China'
  },
  {
    mountain: 'Škrlatica',
    metres: '2,740',
    range: 'Julian Alps',
    location: 'Slovenia'
  },
  {
    mountain: 'Monte Roraima',
    metres: '2,739',
    range: 'Serra de Pacaraima',
    location: 'Roraima, Brazil'
  },
  {
    mountain: 'Heavens Peak',
    metres: '2,739',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Mount Spickard',
    metres: '2,737',
    range: 'North Cascades',
    location: 'US'
  },
  {
    mountain: 'Banderishki Chukar',
    metres: '2,732',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Melbourne',
    metres: '2,732',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Cerro El Pital',
    metres: '2,730',
    range: '',
    location: 'El Salvador'
  },
  {
    mountain: 'Mount Redoubt',
    metres: '2,730',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Dzhengal',
    metres: '2,730',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Malyovitsa',
    metres: '2,729',
    range: 'Rila Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Pizzo di Claro',
    metres: '2,727',
    range: 'Ticino, Switzerland',
    location: ''
  },
  {
    mountain: 'Mount Morning',
    metres: '2,723',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Phu Xai Lai Leng',
    metres: '2,720',
    range: '',
    location: 'Laos'
  },
  {
    mountain: 'Serles',
    metres: '2,718',
    range: 'Stubai Alps',
    location: 'Austria'
  },
  {
    mountain: 'Mount Singakalsa (Timbak)',
    metres: '2,717',
    range: 'Cordillera Range',
    location: 'Benguet, Philippines'
  },
  {
    mountain: 'Watzmann',
    metres: '2,713',
    range: 'Bavarian Alps',
    location: 'Germany'
  },
  {
    mountain: 'Boston Peak',
    metres: '2,711',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Schiahorn',
    metres: '2,709',
    range: '',
    location: 'Switzerland'
  },
  {
    mountain: 'Grotto Mountain',
    metres: '2,706',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Mount Haku',
    metres: '2,702',
    range: 'Ryōhaku Mountains',
    location: 'Japan'
  },
  {
    mountain: 'Eldorado Peak',
    metres: '2,701',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Anaimudi',
    metres: '2,695',
    range: '',
    location: 'Kerala, India'
  },
  {
    mountain: 'Maja Jezerce',
    metres: '2,694',
    range: '#1 in Dinaric Alps',
    location: 'Albania'
  },
  {
    mountain: 'Half Dome',
    metres: '2,693',
    range: 'Sierra Nevada',
    location: 'California, US'
  },
  {
    mountain: 'Mount Louis',
    metres: '2,682',
    range: '',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Mount Discovery',
    metres: '2,681',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Morro do Couto',
    metres: '2,680',
    range: 'Serra das Prateleiras',
    location: 'Rio de Janeiro, Brazil'
  },
  {
    mountain: 'Pic de la Selle',
    metres: '2,680',
    range: '',
    location: 'Haiti'
  },
  {
    mountain: 'Mount Garibaldi',
    metres: '2,678',
    range: 'British Columbia',
    location: 'Canada'
  },
  {
    mountain: 'Bashliyski Chukar',
    metres: '2,670',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Pedra do Sino',
    metres: '2,670',
    range: 'Serra dos Órgãos',
    location: 'Rio de Janeiro, Brazil'
  },
  {
    mountain: 'Guadalupe Peak',
    metres: '2,667',
    range: 'Guadalupe Mountains',
    location: 'Texas'
  },
  {
    mountain: 'Gjeravica',
    metres: '2,656',
    range: 'Accursed Mountains',
    location: 'Kosovo'
  },
  {
    mountain: 'Großer Krottenkopf',
    metres: '2,656',
    range: 'Allgäu Alps',
    location: 'Tyrol, Austria'
  },
  {
    mountain: 'Pic del Port Vell',
    metres: '2,655',
    range: 'Pyrenees',
    location: 'Andorra'
  },
  {
    mountain: 'Gerlachov Peak',
    metres: '2,655',
    range: 'High Tatras',
    location: 'Slovakia'
  },
  {
    mountain: 'Mount Michelson',
    metres: '2,652',
    range: 'Chugach Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Torre Cerredo',
    metres: '2,650',
    range: 'Picos de Europa',
    location: 'Spain'
  },
  {
    mountain: 'Mount St. Piran',
    metres: '2,649',
    range: 'Bow Range',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Mount Meager',
    metres: '2,645',
    range: 'British Columbia',
    location: 'Canada'
  },
  {
    mountain: 'Mount Trus Madi',
    metres: '2,642',
    range: 'Trus Madi Range',
    location: 'Sabah, Malaysia'
  },
  {
    mountain: 'Meesapulimala',
    metres: '2,640',
    range: 'Tamil Nadu/Kerela',
    location: 'India'
  },
  {
    mountain: 'Doddabetta',
    metres: '2,637',
    range: 'Tamil Nadu',
    location: 'India'
  },
  {
    mountain: 'Piton de la Fournaise',
    metres: '2,631',
    range: '',
    location: 'Réunion'
  },
  {
    mountain: 'Mount Crean',
    metres: '2,630',
    range: 'Victoria Land',
    location: 'Antarctica'
  },
  {
    mountain: 'Naina Peak (China Peak)',
    metres: '2,619',
    range: 'Uttarakhand',
    location: 'India'
  },
  {
    mountain: 'Cardinal Peak',
    metres: '2,618',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Peña Vieja',
    metres: '2,617',
    range: 'Picos de Europa',
    location: 'Cantabria, Spain'
  },
  {
    mountain: 'Barbeau Peak',
    metres: '2,616',
    range: 'Nunavut',
    location: 'Canada'
  },
  {
    mountain: 'Pelister Peak',
    metres: '2,601',
    range: '',
    location: 'North Macedonia'
  },
  {
    mountain: 'Mount Abao',
    metres: '2,596',
    range: '',
    location: 'Philippines'
  },
  { mountain: 'Pico Almanzor', metres: '2,592', range: '', location: 'Spain' },
  {
    mountain: 'Mount Currie',
    metres: '2,591',
    range: '',
    location: 'BC, Canada'
  },
  {
    mountain: 'Pizzo Molare',
    metres: '2,585',
    range: 'Ticino',
    location: 'Switzerland'
  },
  {
    mountain: 'Mount Tavkvetili',
    metres: '2,583',
    range: '',
    location: 'Georgia'
  },
  {
    mountain: 'Mount Tambuyukon',
    metres: '2,579',
    range: 'Sabah',
    location: 'Malaysia'
  },
  {
    mountain: 'Argonaut Peak',
    metres: '2,576',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Doi Inthanon',
    metres: '2,565',
    range: '',
    location: 'Thailand'
  },
  {
    mountain: "Cooke's Peak/Cook's Peak",
    metres: '2,563',
    range: '',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Pic dels Aspres',
    metres: '2,562',
    range: '',
    location: 'Pyrenees, Andorra'
  },
  { mountain: 'Ledyanaya', metres: '2,562', range: '', location: 'Russia' },
  {
    mountain: 'Aguja Saint Exupery',
    metres: '2,558',
    range: '',
    location: 'Argentina'
  },
  {
    mountain: 'Mount St. Helens',
    metres: '2,550',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Big Hatchet Peak',
    metres: '2,547',
    range: '',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Moldoveanu Peak',
    metres: '2,544',
    range: '',
    location: 'Romania'
  },
  {
    mountain: 'Mount Vineuo',
    metres: '2,536',
    range: '',
    location: 'Papua New Guinea'
  },
  { mountain: 'Negoiu Peak', metres: '2,535', range: '', location: 'Romania' },
  { mountain: 'Vistea Mare', metres: '2,527', range: '', location: 'Romania' },
  {
    mountain: 'Pidurutalagala',
    metres: '2,524',
    range: '',
    location: 'Sri Lanka'
  },
  {
    mountain: 'Mount Norquay',
    metres: '2,522',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  { mountain: 'Hualalai', metres: '2,521', range: '', location: 'Hawaii, US' },
  {
    mountain: 'Mount Yesenin',
    metres: '2,520',
    range: '',
    location: 'Antarctica'
  },
  { mountain: 'Parangu Mare', metres: '2,519', range: '', location: 'Romania' },
  {
    mountain: 'Mount Taranaki/Egmont',
    metres: '2,518',
    range: '',
    location: 'New Zealand'
  },
  {
    mountain: 'Garfield Peak',
    metres: '2,512',
    range: '',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Joern',
    metres: '2,510',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Veniaminof',
    metres: '2,507',
    range: '',
    location: 'Alaska, US'
  },
  { mountain: 'Omu Peak', metres: '2,505', range: '', location: 'Romania' },
  {
    mountain: 'Rysy',
    metres: '2,503',
    range: 'Tatras',
    location: 'Poland/Slovakia'
  },
  {
    mountain: 'Mount Tymfi',
    metres: '2,497',
    range: 'Pindus',
    location: 'Greece'
  },
  {
    mountain: 'Monte Velino',
    metres: '2,487',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Monte Vettore',
    metres: '2,476',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Špik',
    metres: '2,472',
    range: 'Julian Alps',
    location: 'Slovenia'
  },
  {
    mountain: 'Luna Peak',
    metres: '2,470',
    range: '',
    location: 'British Columbia, Canada'
  },
  { mountain: 'Galdhøpiggen', metres: '2,469', range: '', location: 'Norway' },
  {
    mountain: 'Kanlaon',
    metres: '2,465',
    range: 'Negros Occidental and Negros Oriental',
    location: 'Philippines'
  },
  { mountain: 'Glittertind', metres: '2,464', range: '', location: 'Norway' },
  { mountain: 'El Capitan', metres: '2,464', range: '', location: 'Texas, US' },
  { mountain: 'Shimbiris', metres: '2,464', range: '', location: 'Somalia' },
  {
    mountain: 'Mayon',
    metres: '2,463',
    range: '',
    location: 'Albay, Philippines'
  },
  {
    mountain: 'Monte Gorzano',
    metres: '2,458',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Mount Psiloritis',
    metres: '2,456',
    range: '',
    location: 'Crete, Greece'
  },
  {
    mountain: 'Mount Pachnes',
    metres: '2,453',
    range: 'Lefka Ori',
    location: 'Crete, Greece'
  },
  {
    mountain: 'Sulphur Mountain',
    metres: '2,451',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Sunset Crater',
    metres: '2,451',
    range: '',
    location: 'Arizona, US'
  },
  {
    mountain: 'Cima del Redentore (Sibillini Mountains)',
    metres: '2,448',
    range: '',
    location: 'Marche - Umbria Italy'
  },
  {
    mountain: 'Faraya Mzaar',
    metres: '2,444',
    range: '',
    location: 'Keserwan, Lebanon'
  },
  {
    mountain: 'Triple Divide Peak',
    metres: '2,444',
    range: '',
    location: 'Montana, US'
  },
  {
    mountain: 'Blackcomb Peak',
    metres: '2,436',
    range: '',
    location: 'Whistler BC, Canada'
  },
  {
    mountain: 'Monte Binga',
    metres: '2,436',
    range: '',
    location: 'Manica, Mozambique'
  },
  {
    mountain: 'Mount Olympus',
    metres: '2,432',
    range: '',
    location: 'Washington, US'
  },
  { mountain: 'Peñalara', metres: '2,430', range: '', location: 'Spain' },
  {
    mountain: 'Mesa de los Tres Reyes',
    metres: '2,428',
    range: '',
    location: 'Navarre, Spain'
  },
  {
    mountain: 'Mount Daniel',
    metres: '2,426',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Murud',
    metres: '2,423',
    range: 'Kelabit Highlands',
    location: 'Sarawak, Malaysia'
  },
  {
    mountain: 'Mocho',
    metres: '2,422',
    range: 'Los Ríos Region',
    location: 'Chile'
  },
  {
    mountain: 'Choshuenco',
    metres: '2,415',
    range: 'Los Ríos Region',
    location: 'Chile'
  },
  {
    mountain: 'Ivis Peak',
    metres: '2,414',
    range: 'Los Ríos Region',
    location: 'Chile'
  },
  {
    mountain: 'Ha Ling Peak',
    metres: '2,408',
    range: 'Canadian Rockies',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Store Skagastølstind',
    metres: '2,405',
    range: '',
    location: 'Norway'
  },
  { mountain: 'Hajla', metres: '2,403', range: '', location: 'Kosovo' },
  {
    mountain: 'Le Tabor',
    metres: '2,389',
    range: 'Dauphine Alps',
    location: 'France'
  },
  {
    mountain: 'Kirigalpottha',
    metres: '2,388',
    range: '',
    location: 'Sri Lanka'
  },
  {
    mountain: 'Maglić',
    metres: '2,386',
    range: '',
    location: 'Bosnia and Herzegovina'
  },
  { mountain: 'Emory Peak', metres: '2,385', range: '', location: 'Texas, US' },
  {
    mountain: 'Mount Cayley',
    metres: '2,385',
    range: '',
    location: 'British Columbia Canada'
  },
  {
    mountain: 'Mount Galwey',
    metres: '2,377',
    range: 'Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Botev Peak',
    metres: '2,376',
    range: 'Balkan Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Mulu',
    metres: '2,376',
    range: 'Gunung Mulu National Park',
    location: 'Sarawak, Malaysia'
  },
  {
    mountain: 'Mount Deception',
    metres: '2,374',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Aix',
    metres: '2,367',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Karthala',
    metres: '2,361',
    range: '',
    location: 'Comoros, Indian Ocean'
  },
  {
    mountain: 'Totapala Kanda',
    metres: '2,357',
    range: '',
    location: 'Sri Lanka'
  },
  {
    mountain: 'Signal Mountain',
    metres: '2,353',
    range: '',
    location: 'Wyoming, US'
  },
  {
    mountain: 'Mount Pico',
    metres: '2,351',
    range: '',
    location: 'Azores, Portugal'
  },
  {
    mountain: 'Grimming',
    metres: '2,351',
    range: '',
    location: 'Styria, Austria'
  },
  {
    mountain: 'Monte Sirente',
    metres: '2,347',
    range: '',
    location: 'Abruzzo Italy'
  },
  {
    mountain: 'Mount Blum',
    metres: '2,340',
    range: 'North Cascades',
    location: 'US'
  },
  {
    mountain: 'Mount Popomanaseu',
    metres: '2,335',
    range: 'Guadalcanal',
    location: 'Solomon Islands'
  },
  {
    mountain: 'Monte Priora',
    metres: '2,333',
    range: '',
    location: 'Marche Italy'
  },
  {
    mountain: 'Mount Prophet',
    metres: '2,330',
    range: '',
    location: 'North Cascades'
  },
  {
    mountain: 'Mount Griggs',
    metres: '2,317',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Makarakomburu',
    metres: '2,310',
    range: 'Guadalcanal',
    location: 'Solomon Islands'
  },
  {
    mountain: 'Lalla Khedidja',
    metres: '2,308',
    range: 'Djurdjura Mountains',
    location: 'Algeria'
  },
  {
    mountain: 'Lavender Peak',
    metres: '2,306',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Pietrosul Rodnei Peak',
    metres: '2,303',
    range: 'Rodnei Mountains, Romanian Carpathian Mountains',
    location: 'Romania'
  },
  {
    mountain: 'Mount Sir Allan MacNab',
    metres: '2,297',
    range: 'Premier Range',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Murree',
    metres: '2,291',
    range: '',
    location: 'Rawalpindi, Pakistan'
  },
  {
    mountain: 'Kozi Wierch',
    metres: '2,291',
    range: '',
    location: 'Tatras, Poland'
  },
  {
    mountain: 'Cherni Vrah',
    metres: '2,290',
    range: 'Vitosha Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Druesberg',
    metres: '2,282',
    range: 'Swiss Alps',
    location: 'Switzerland'
  },
  {
    mountain: 'Ineu Peak',
    metres: '2,279',
    range: 'Rodnei Mountains, Romanian Carpathian Mountains',
    location: 'Romania'
  },
  { mountain: 'Mount Huye', metres: '2,278', range: '', location: 'Rwanda' },
  { mountain: 'Bikku Bitti', metres: '2,267', range: '', location: 'Libya' },
  {
    mountain: 'Blue Mountains',
    metres: '2,256',
    range: '',
    location: 'Jamaica'
  },
  {
    mountain: 'Mount Dietz',
    metres: '2,250',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Sri Pada Mountain',
    metres: '2,243',
    range: '',
    location: 'Sri Lanka'
  },
  {
    mountain: 'Mount John Laurie',
    metres: '2,240',
    range: 'Canadian Rockies',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Red Butte',
    metres: '2,232',
    range: '',
    location: 'Arizona, US'
  },
  {
    mountain: 'Čvrsnica',
    metres: '2,228',
    range: '',
    location: 'Herzegovina, BiH'
  },
  {
    mountain: 'Mount Kosciuszko',
    metres: '2,228',
    range: 'Snowy Mountains',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Camoghe',
    metres: '2,228',
    range: '',
    location: 'Ticino, Switzerland'
  },
  {
    mountain: 'Commonwealth Mountain',
    metres: '2,225',
    range: 'Nunavut',
    location: 'Canada'
  },
  {
    mountain: 'Mount Chiginagak',
    metres: '2,221',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Monte Terminillo',
    metres: '2,217',
    range: '',
    location: 'Lazio Italy'
  },
  {
    mountain: 'Slavyanka',
    metres: '2,212',
    range: 'Pirin Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Mount Oxford',
    metres: '2,210',
    range: 'Nunavut',
    location: 'Canada'
  },
  {
    mountain: 'Mount Townsend',
    metres: '2,209',
    range: 'Snowy Mountains',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Black Elk Peak',
    metres: '2,207',
    range: '',
    location: 'South Dakota, US'
  },
  {
    mountain: 'Golden Hinde',
    metres: '2,198',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Mount Twynam',
    metres: '2,196',
    range: 'Snowy Mountains',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Elkhorn Mountain',
    metres: '2,195',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Mount Baldy',
    metres: '2,192',
    range: '',
    location: 'Alberta, Canada'
  },
  {
    mountain: 'Golyam Perelik',
    metres: '2,191',
    range: 'Rhodope Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Shiprock',
    metres: '2,188',
    range: '',
    location: 'New Mexico, US'
  },
  {
    mountain: 'Mount Tahan',
    metres: '2,187',
    range: 'Tahan Range',
    location: 'Pahang, Malaysia'
  },
  {
    mountain: 'Mount Korbu',
    metres: '2,183',
    range: 'Titiwangsa Mountains',
    location: 'Perak, Malaysia'
  },
  {
    mountain: 'Mount Yong Belar',
    metres: '2,180',
    range: '',
    location: 'Perak, Malaysia'
  },
  {
    mountain: 'Doi Chiang Dao',
    metres: '2,175',
    range: '',
    location: 'Thailand'
  },
  {
    mountain: 'Galunggung',
    metres: '2,168',
    range: 'Java',
    location: 'Indonesia'
  },
  {
    mountain: 'Levski Peak',
    metres: '2,166',
    range: 'Balkan Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Monte Cimone',
    metres: '2,165',
    range: '',
    location: 'Emilia-Romagna Italy'
  },
  {
    mountain: 'Mount Hua',
    metres: '2,155',
    range: '',
    location: 'Shaanxi, China'
  },
  {
    mountain: 'Mount Ugo',
    metres: '2,150',
    range: 'Cordillera Range',
    location: 'Benguet, Philippines'
  },
  { mountain: 'Mount Zulia', metres: '2,149', range: '', location: 'Uganda' },
  {
    mountain: 'Carruthers Peak',
    metres: '2,145',
    range: 'Snowy Mountains',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Crna Glava',
    metres: '2,139',
    range: 'Bjelasica, Crna Gora',
    location: 'Montenegro'
  },
  {
    mountain: 'Mount Colonel Foster',
    metres: '2,135',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Roche à Perdrix',
    metres: '2,135',
    range: 'Fiddle Range, Canadian Rockies',
    location: 'Canada'
  },
  {
    mountain: 'Kodaikanal',
    metres: '2,133',
    range: '',
    location: 'Tamil Nadu, India'
  },
  { mountain: 'Pilatus', metres: '2,132', range: '', location: 'Switzerland' },
  {
    mountain: 'Strmenica',
    metres: '2,122',
    range: 'Bjelasica, Crna Gora',
    location: 'Montenegro'
  },
  {
    mountain: 'Phu Soi Dao',
    metres: '2,120',
    range: '',
    location: 'Thailand/Laos'
  },
  {
    mountain: 'Eagle Peak',
    metres: '2,119',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Zekova Glava',
    metres: '2,117',
    range: 'Bjelasica, Crna Gora',
    location: 'Montenegro'
  },
  { mountain: 'Öræfajökull', metres: '2,110', range: '', location: 'Iceland' },
  { mountain: 'Mount Benum', metres: '2,107', range: '', location: 'Malaysia' },
  { mountain: 'Mogotón', metres: '2,107', range: '', location: 'Nicaragua' },
  { mountain: 'Kebnekaise', metres: '2,106', range: '', location: 'Sweden' },
  {
    mountain: 'Rambler Peak',
    metres: '2,092',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Mount McBride',
    metres: '2,083',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Pico do Selado',
    metres: '2,082',
    range: 'Monte Verde, Minas Gerais',
    location: 'Brazil'
  },
  {
    mountain: 'Phou Khe',
    metres: '2,079',
    range: '',
    location: 'Thailand/Laos'
  },
  {
    mountain: 'Schneeberg',
    metres: '2,076',
    range: 'Northern Limestone Alps',
    location: 'Austria'
  },
  {
    mountain: 'Mount Tate',
    metres: '2,068',
    range: 'Snowy Mountains',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Bjelašnica',
    metres: '2,067',
    range: 'Sarajevo Canton',
    location: 'Bosnia and Herzegovina'
  },
  {
    mountain: 'Kings Peak',
    metres: '2,065',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  { mountain: 'Hoverla', metres: '2,061', range: '', location: 'Ukraine' },
  {
    mountain: 'Monte Miletto',
    metres: '2,050',
    range: '',
    location: 'Molise Italy'
  },
  {
    mountain: 'Lytton Mountain',
    metres: '2,049',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Mount Katmai',
    metres: '2,047',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Dimlang',
    metres: '2,042',
    range: '',
    location: 'Adamawa, Nigeria'
  },
  {
    mountain: 'Mount Celeste',
    metres: '2,041',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Mount Mitchell',
    metres: '2,037',
    range: '',
    location: 'Yancey County, North Carolina, US'
  },
  { mountain: 'Namunukula', metres: '2,035', range: '', location: 'Sri Lanka' },
  {
    mountain: 'Pico do Barbado',
    metres: '2,033',
    range: '',
    location: 'Bahia, Brazil'
  },
  {
    mountain: 'Mount Batu Brinchang',
    metres: '2,032',
    range: 'Cameron Highlands',
    location: 'Pahang/Perak, Malaysia'
  },
  { mountain: 'Doi Mae Tho', metres: '2,031', range: '', location: 'Thailand' },
  {
    mountain: 'Clingmans Dome',
    metres: '2,025',
    range: 'Great Smoky Mountains',
    location: 'Tennessee, US'
  },
  {
    mountain: 'Mount Heng (Shanxi)',
    metres: '2,017',
    range: '',
    location: 'Shanxi, China'
  },
  {
    mountain: 'Kopaonik',
    metres: '2,017',
    range: "Pančić's Peak",
    location: 'Serbia'
  },
  {
    mountain: 'Alpe di Succiso',
    metres: '2,017',
    range: '',
    location: 'Province of Reggio Emilia Italy'
  },
  {
    mountain: 'Kom',
    metres: '2,016',
    range: 'Balkan Mountain',
    location: 'Bulgaria'
  },
  {
    mountain: 'Polar Bear Peak',
    metres: '2,016',
    range: 'Chugach Mountains',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Asgard',
    metres: '2,015',
    range: 'Baffin Mountains',
    location: 'Nunavut, Canada'
  },
  {
    mountain: 'Mount Le Conte',
    metres: '2,010',
    range: 'Great Smoky Mountains',
    location: 'Tennessee, US'
  },
  {
    mountain: 'Serra da Estrela',
    metres: '1,993',
    range: '',
    location: 'Portugal'
  },
  {
    mountain: 'Mount Bogong',
    metres: '1,986',
    range: '',
    location: 'Australia'
  },
  {
    mountain: 'Mount Ishizuchi',
    metres: '1,982',
    range: '',
    location: 'Japan'
  },
  {
    mountain: 'Doi Phu Kha',
    metres: '1,980',
    range: 'Luang Prabang Range',
    location: 'Thailand'
  },
  {
    mountain: 'Pizzo Carbonara',
    metres: '1,979',
    range: 'Province of Palermo Sicily',
    location: 'Italy'
  },
  {
    mountain: 'Topo de Coroa',
    metres: '1,979',
    range: '',
    location: 'Cape Verde'
  },
  {
    mountain: 'Mount Tongariro',
    metres: '1,978',
    range: '',
    location: 'New Zealand'
  },
  {
    mountain: 'Iceberg Peak',
    metres: '1,977',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  { mountain: 'Pico Turquino', metres: '1,975', range: '', location: 'Cuba' },
  {
    mountain: 'El Piveto Mountain',
    metres: '1,969',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Qiajivik Mountain',
    metres: '1,963',
    range: '',
    location: 'Baffin Island, Nunavut, Canada'
  },
  {
    mountain: 'Monte Tamaro',
    metres: '1,962',
    range: '',
    location: 'Ticino, Switzerland'
  },
  { mountain: 'Farcau Peak', metres: '1,956', range: '', location: 'Romania' },
  {
    mountain: 'Montalto (Aspromonte)',
    metres: '1,955',
    range: '',
    location: 'Calabria Italy'
  },
  { mountain: 'Ciucas Peak', metres: '1,954', range: '', location: 'Romania' },
  { mountain: 'Chionistra', metres: '1,952', range: '', location: 'Cyprus' },
  {
    mountain: 'Hallasan',
    metres: '1,950',
    range: '',
    location: 'Jejudo, South Korea'
  },
  { mountain: 'Jabal Bil Ays', metres: '1,934', range: '', location: 'Oman' },
  {
    mountain: 'Mullayanagiri',
    metres: '1,930',
    range: 'Chikmagalur',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Helvetia Tinde',
    metres: '1,929',
    range: 'Roosevelt Range',
    location: 'Northern Greenland'
  },
  {
    mountain: 'Botte Donato',
    metres: '1,928',
    range: '',
    range: 'Sila Mountains',
    location: 'Calabria, Italy'
  },
  {
    mountain: 'Mount Kirigamine',
    metres: '1,925',
    range: '',
    location: 'Japan'
  },
  {
    mountain: 'Mount Washington',
    metres: '1,917',
    range: 'Presidential Range, White Mtns., Appalachian Mountains',
    location: 'US'
  },
  {
    mountain: 'Mount Siku',
    metres: '1,915',
    range: '',
    location: 'Pahang, Malaysia'
  },
  {
    mountain: 'Mount Jiri',
    metres: '1,915',
    range: '',
    location: 'South Korea'
  },
  {
    mountain: 'Troglav, Dinara',
    metres: '1,913',
    range: '',
    location: 'Bosnia and Herzegovina'
  },
  { mountain: 'Mont Ventoux', metres: '1,909', range: '', location: 'France' },
  { mountain: 'Ocolasu Mare', metres: '1,907', range: '', location: 'Romania' },
  { mountain: 'Mount Toaca', metres: '1,900', range: '', location: 'Romania' },
  {
    mountain: 'Vesper Peak',
    metres: '1,896',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Baba Budangiri',
    metres: '1,895',
    range: 'Chikmagalur',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Narodnaya',
    metres: '1,895',
    range: 'Subpolar Urals',
    location: 'Russia'
  },
  {
    mountain: 'Kudremukh',
    metres: '1,894',
    range: 'Chikmagalur',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Orjen',
    metres: '1,894',
    range: '',
    location: 'Montenegro, Bosnia and Herzegovina'
  },
  {
    mountain: 'Iskhodnaya',
    metres: '1,887',
    range: 'Chukotka Mountains',
    location: 'Russia'
  },
  {
    mountain: 'Mount George V',
    metres: '1,883',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Rugged Mountain',
    metres: '1,875',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Agastya Mala',
    metres: '1,868',
    range: '',
    location: 'Tamil Nadu/Kerala, India'
  },
  {
    mountain: 'Trident Volcano',
    metres: '1,864',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Martin',
    metres: '1,863',
    range: '',
    location: 'Alaska, US'
  },
  { mountain: 'Knuckles', metres: '1,862', range: '', location: 'Sri Lanka' },
  {
    mountain: 'Pico Ruivo',
    metres: '1,861',
    range: '',
    location: 'Madeira, Portugal'
  },
  { mountain: 'Budacu', metres: '1,859', range: '', location: 'Romania' },
  { mountain: 'Giumalau', metres: '1,856', range: '', location: 'Romania' },
  {
    mountain: 'Pico das Torres',
    metres: '1,853',
    range: '',
    location: 'Madeira, Portugal'
  },
  {
    mountain: 'Mont Ross',
    metres: '1,850',
    range: 'Gallieni Massif',
    location: 'Kerguelen Islands'
  },
  {
    mountain: 'Cucurbata Mare',
    metres: '1,849',
    range: '',
    location: 'Romania'
  },
  {
    mountain: 'Monte Soro',
    metres: '1,847',
    range: 'Province of Messina Sicily',
    location: 'Italy'
  },
  {
    mountain: 'Mount Ulap',
    metres: '1,846',
    range: 'Itogon',
    location: 'Benguet, Philippines'
  },
  {
    mountain: 'Crown Mountain',
    metres: '1,846',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  { mountain: 'Tibles', metres: '1,839', range: '', location: 'Romania' },
  { mountain: 'Loser', metres: '1,838', range: '', location: 'Austria' },
  { mountain: 'Vladeasa', metres: '1,836', range: '', location: 'Romania' },
  {
    mountain: 'Punta La Marmora',
    metres: '1,834',
    range: 'Sardinia',
    location: 'Italy'
  },
  {
    mountain: 'Sinjal',
    metres: '1,831',
    range: 'Mountain Dinara',
    location: 'Croatia'
  },
  {
    mountain: 'Cerro La Campana',
    metres: '1,828',
    range: 'Olmue',
    location: 'Chile'
  },
  {
    mountain: 'Gora Nevskaya',
    metres: '1,828',
    range: 'Magadan Oblast',
    location: 'Russia'
  },
  { mountain: 'Muntele Mare', metres: '1,826', range: '', location: 'Romania' },
  {
    mountain: 'Cerro de la Silla',
    metres: '1,820',
    range: '',
    location: 'Mexico'
  },
  {
    mountain: 'Pico do Arieiro',
    metres: '1,818',
    range: '',
    location: 'Madeira, Portugal'
  },
  {
    mountain: 'Biligiriranga Hills',
    metres: '1,800',
    range: 'Chamarajanagar',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Monte Maggiorasca',
    metres: '1,799',
    range: '',
    location: 'liguria - Emilia-Romagna Italy'
  },
  {
    mountain: 'Rigi',
    metres: '1,797',
    range: 'Swiss Alps',
    location: 'Switzerland'
  },
  { mountain: 'Hășmașu Mare', metres: '1,792', range: '', location: 'Romania' },
  {
    mountain: 'Pietrosu Peak',
    metres: '1,791',
    range: '',
    location: 'Romania'
  },
  { mountain: 'Goru', metres: '1,784', range: '', location: 'Romania' },
  {
    mountain: 'Blokhin Peak',
    metres: '1,779',
    range: 'Chukotka',
    location: 'Russian'
  },
  {
    mountain: 'Velliangiri Mountains',
    metres: '1,778',
    range: 'Coimbatore',
    location: 'Tamil Nadu, India'
  },
  { mountain: 'Lacaut', metres: '1,777', range: '', location: 'Romania' },
  { mountain: 'Saca Peak', metres: '1,776', range: '', location: 'Romania' },
  { mountain: 'Penteleu', metres: '1,772', range: '', location: 'Romania' },
  {
    mountain: 'Mount Veve',
    metres: '1,768',
    range: '',
    location: 'Kolombangara, Solomon Islands'
  },
  {
    mountain: 'Mount Adams',
    metres: '1,766',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Monte Gariglione',
    metres: '1,764',
    range: 'Sila Mountains',
    location: 'Calabria, Italy'
  },
  {
    mountain: 'Sveti Jure (Saint George)',
    metres: '1,762',
    range: 'Mountain Biokovo',
    location: 'Croatia'
  },
  {
    mountain: 'Pik Sovetskoy Gvardii',
    metres: '1,759',
    range: 'Chukotka',
    location: 'Russian'
  },
  {
    mountain: 'Vaganski vrh',
    metres: '1,757',
    range: 'Mountain Velebit',
    location: 'Croatia'
  },
  {
    mountain: 'Mount Api',
    metres: '1,750',
    range: 'Gunung Mulu National Park',
    location: 'Sarawak, Malaysia'
  },
  { mountain: 'Lovćen', metres: '1,749', range: '', location: 'Montenegro' },
  {
    mountain: 'Tadiandamol',
    metres: '1,748',
    range: 'Kodagu',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Mount Rogers',
    metres: '1,746',
    range: '',
    location: 'Virginia, US'
  },
  {
    mountain: 'Doi Phi Pan Nam',
    metres: '1,745',
    range: 'Luang Prabang Range',
    location: 'Thailand'
  },
  {
    mountain: 'Mount Wilson',
    metres: '1,742',
    range: '',
    location: 'California, US'
  },
  {
    mountain: 'Mount Jefferson',
    metres: '1,741',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Monte Amiata',
    metres: '1,738',
    range: '',
    location: 'Province of Grosseto Italy'
  },
  { mountain: 'Daisen', metres: '1,729', range: '', location: 'Japan' },
  {
    mountain: 'The Horn (Mount Buffalo)',
    metres: '1,723',
    range: '',
    location: 'Victoria, Australia'
  },
  {
    mountain: 'Mount Batur',
    metres: '1,717',
    range: '',
    location: 'Bali, Indonesia'
  },
  {
    mountain: 'Kumara Parvata',
    metres: '1,712',
    range: 'Dakshina Kannada',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Pushpagiri',
    metres: '1,712',
    range: 'Pushpagiri Wildlife Sanctuary',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Mount Lowe',
    metres: '1,707',
    range: 'San Gabriel Mountains',
    location: 'California, US'
  },
  {
    mountain: 'Boulder Peak',
    metres: '1,707',
    range: 'Olympic Mountains',
    location: 'Washington, US'
  },
  {
    mountain: 'Doi Luang',
    metres: '1,694',
    range: 'Phi Pan Nam',
    location: 'Thailand'
  },
  {
    mountain: 'Mitre Peak',
    metres: '1,692',
    range: '',
    location: 'New Zealand'
  },
  { mountain: 'Arma Konda', metres: '1,680', range: '', location: 'India' },
  { mountain: 'Deomali', metres: '1,672', range: '', location: 'India' },
  {
    mountain: 'Mount Caubvik',
    metres: '1,652',
    range: 'Torngat Mountains',
    location: 'Canada'
  },
  { mountain: 'Kalsubai', metres: '1,646', range: '', location: 'India' },
  { mountain: 'Rarau', metres: '1,650', range: '', location: 'Romania' },
  { mountain: 'Peak 5390', metres: '1,643', range: '', location: 'Alaska, US' },
  {
    mountain: 'Yamantau',
    metres: '1,640',
    range: '',
    location: 'Southern Urals, Russia'
  },
  {
    mountain: 'Mount Monroe',
    metres: '1,637',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Mount Madison',
    metres: '1,636',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Mount Krebs',
    metres: '1,630',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Marcy',
    metres: '1,629',
    range: '',
    location: 'New York, US'
  },
  {
    mountain: 'Trebević',
    metres: '1,627',
    range: '',
    location: 'Bosnia and Herzegovina'
  },
  {
    mountain: 'Mount Bartle Frere',
    metres: '1,622',
    range: '',
    location: 'Queensland, Australia'
  },
  {
    mountain: 'Rocca Busambra',
    metres: '1,613',
    range: '',
    location: 'Province of Palermo Sicily'
  },
  {
    mountain: 'Velky Rozsutec ',
    metres: '1,610',
    range: '',
    location: 'Mala Fatra, Slovakia'
  },
  {
    mountain: 'Brahmagiri',
    metres: '1,608',
    range: 'Kodagu',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Mount Katahdin',
    metres: '1,606',
    range: '',
    location: 'Maine, US'
  },
  {
    mountain: 'Sněžka',
    metres: '1,602',
    range: 'Krkonoše',
    location: 'Czech Republic'
  },
  {
    mountain: 'Sirumalai',
    metres: '1,600',
    range: '',
    location: 'Tamil Nadu India'
  },
  {
    mountain: 'Mount Lafayette',
    metres: '1,600',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Mount Washington (British Columbia)',
    metres: '1,590',
    range: '',
    location: 'Vancouver Island, British Columbia, Canada'
  },
  {
    mountain: 'Mount Benarat',
    metres: '1,585',
    range: 'Gunung Mulu National Park',
    location: 'Sarawak, Malaysia'
  },
  {
    mountain: "Wai'ale'ale",
    metres: '1,569',
    range: '',
    location: 'Kauai, Hawaii, US'
  },
  {
    mountain: 'Salher',
    metres: '1,567',
    range: '',
    location: 'Maharashtra, India'
  },
  {
    mountain: 'Cirque Mountain',
    metres: '1,567',
    range: '',
    location: 'Labrador, Canada'
  },
  {
    mountain: 'Algonquin Peak',
    metres: '1,559',
    range: '',
    location: 'New York, US'
  },
  {
    mountain: 'Mount Lincoln',
    metres: '1,551',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'Superstition Mountain',
    metres: '1,542',
    range: '',
    location: 'Arizona, US'
  },
  {
    mountain: 'Mount Tai',
    metres: '1,532',
    range: '',
    location: 'Shandong, China'
  },
  {
    mountain: 'Jabel Yibir',
    metres: '1,527',
    range: '',
    location: 'United Arab Emirates'
  },
  {
    mountain: 'Madikeri',
    metres: '1,525',
    range: 'Kodagu',
    location: 'Karnataka, India'
  },
  { mountain: 'Mount Popa', metres: '1,518', range: '', location: 'Myanmar' },
  { mountain: 'Askja', metres: '1,516', range: '', location: 'Iceland' },
  {
    mountain: 'Monte Boglia',
    metres: '1,516',
    range: '',
    location: 'Ticino, Switzerland'
  },
  {
    mountain: 'Kamakau',
    metres: '1,512',
    range: '',
    location: 'Molokai, Hawaii, US'
  },
  {
    mountain: 'Song Shan',
    metres: '1,512',
    range: '',
    location: 'Henan, China'
  },
  {
    mountain: 'Pico de Malpaso',
    metres: '1,501',
    range: '',
    location: 'Canary Islands, Spain'
  },
  { mountain: 'Javaleon', metres: '1,494', range: '', location: 'Spain' },
  {
    mountain: 'Mount Nuang',
    metres: '1,491',
    range: 'Titiwangsa Mountains',
    location: 'Malaysia'
  },
  { mountain: 'Hekla', metres: '1,491', range: '', location: 'Iceland' },
  {
    mountain: 'Mount Pinatubo',
    metres: '1,486',
    range: '',
    location: 'Luzon, Philippines'
  },
  {
    mountain: 'Spruce Knob',
    metres: '1,482',
    range: '',
    location: 'West Virginia, US'
  },
  {
    mountain: 'Lyaskovets Peak',
    metres: '1,473',
    range: '',
    location: 'Antarctica'
  },
  {
    mountain: 'Mount Olympus',
    metres: '1,471',
    range: '',
    location: 'Tasmania, Australia'
  },
  {
    mountain: 'Agriolefkes',
    metres: '1,471',
    range: '',
    location: 'Pelion, Greece'
  },
  {
    mountain: 'La Grande Soufrière',
    metres: '1,467',
    range: '',
    location: 'Guadeloupe, France'
  },
  {
    mountain: 'Monte Penice',
    metres: '1,460',
    range: '',
    location: 'Lombardy - Emilia-Romagna Italy'
  },
  {
    mountain: 'Mount Seymour',
    metres: '1,455',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Himavad Gopalaswamy Betta',
    metres: '1,450',
    range: 'Chamarajanagar',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Snæfellsjökull',
    metres: '1,446',
    range: '',
    location: 'Iceland'
  },
  { mountain: 'Gutai Peak', metres: '1,443', range: '', location: 'Romania' },
  {
    mountain: 'Mount Kalourat',
    metres: '1,435',
    range: '',
    location: 'Malaita, Solomon Islands'
  },
  {
    mountain: 'Mount Woodroffe',
    metres: '1,435',
    range: '',
    location: 'South Australia'
  },
  {
    mountain: 'Cypress Mountain',
    metres: '1,432',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Taramati',
    metres: '1,431',
    range: '',
    location: 'Maharashtra, India'
  },
  {
    mountain: 'Harishchandragad',
    metres: '1,427',
    range: '',
    location: 'Maharashtra, India'
  },
  {
    mountain: 'Tukgahgo Mountain',
    metres: '1,425',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Raireshwar',
    metres: '1,398',
    range: '',
    location: 'Maharashtra, India'
  },
  {
    mountain: 'Taburno Camposauro',
    metres: '1,393',
    range: '',
    location: 'Campania Italy'
  },
  {
    mountain: 'Parasnath',
    metres: '1,366',
    range: '',
    location: 'Jharkhand State, India'
  },
  {
    mountain: 'Dhupgarh',
    metres: '1,350',
    range: '',
    location: 'Madhya Pradesh, India'
  },
  {
    mountain: 'Ben Nevis',
    metres: '1,345',
    range: '',
    location: 'Scotland, United Kingdom'
  },
  {
    mountain: 'Kodachadri',
    metres: '1,343',
    range: 'Shimoga',
    location: 'Karnataka, India'
  },
  {
    mountain: 'Mount Jiuhua',
    metres: '1,342',
    range: '',
    location: 'Anhui, China'
  },
  {
    mountain: 'Mount Aniakchak',
    metres: '1,341',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Mount Mansfield',
    metres: '1,339',
    range: 'Green Mountains',
    location: 'Vermont, US'
  },
  {
    mountain: 'Cerro de Punta',
    metres: '1,338',
    range: '',
    location: 'Jayuya, Puerto Rico'
  },
  {
    mountain: 'Mount Bassie',
    metres: '1,311',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Ben Macdui',
    metres: '1,309',
    range: '',
    location: 'Scotland, United Kingdom'
  },
  {
    mountain: 'Mount Kanaga',
    metres: '1,307',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Sinhagad',
    metres: '1,303',
    range: '',
    location: 'Maharashtra, India'
  },
  {
    mountain: 'Mount Heng (Hunan)',
    metres: '1,300',
    range: '',
    location: 'Hunan, China'
  },
  {
    mountain: 'Braeriach',
    metres: '1,296',
    range: '',
    location: 'Scotland, United Kingdom'
  },
  {
    mountain: 'Rajabasa',
    metres: '1,281',
    range: '',
    location: 'Sumatra, Indonesia'
  },
  { mountain: 'Mount Vesuvius', metres: '1,281', range: '', location: 'Italy' },
  {
    mountain: 'Mount Ophir',
    metres: '1,276',
    range: 'Titiwangsa Mountains',
    location: 'Johor, Malaysia'
  },
  {
    mountain: 'Bailadila',
    metres: '1,276',
    range: '',
    location: 'Chhattisgarh State, India'
  },
  {
    mountain: 'Mount Wellington',
    metres: '1,271',
    range: '',
    location: 'Tasmania, Australia'
  },
  {
    mountain: 'Mount Si',
    metres: '1,270',
    range: '',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Qingcheng',
    metres: '1,260',
    range: '',
    location: 'Sichuan, China'
  },
  { mountain: 'Detunata', metres: '1,258', range: '', location: 'Romania' },
  {
    mountain: 'Mount Donna Buang',
    metres: '1,250',
    range: '',
    location: 'Victoria, Australia'
  },
  {
    mountain: "Camel's Hump",
    metres: '1,244',
    range: '',
    location: 'Vermont, US'
  },
  {
    mountain: 'Cannon Mountain',
    metres: '1,240',
    range: '',
    location: 'New Hampshire, US'
  },
  {
    mountain: 'The Priest',
    metres: '1,238',
    range: 'Blue Ridge Mountains',
    location: 'Virginia, US'
  },
  {
    mountain: 'Aonach Beag',
    metres: '1,234',
    range: '',
    location: 'Scotland, United Kingdom'
  },
  {
    mountain: 'Grouse Mountain',
    metres: '1,231',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: "Ka'ala",
    metres: '1,220',
    range: 'Oahu',
    location: 'Hawaii, US'
  },
  {
    mountain: 'Mount Jerai',
    metres: '1,217',
    range: 'Titiwangsa Mountains',
    location: 'Kedah, Malaysia'
  },
  {
    mountain: 'Mount Meron',
    metres: '1,208',
    range: '',
    location: 'Galilee, Israel'
  },
  {
    mountain: 'Mount Fromme',
    metres: '1,185',
    range: '',
    location: 'British Columbia, Canada'
  },
  {
    mountain: 'Doi Ian',
    metres: '1,174',
    range: 'Phi Pan Nam Range',
    location: 'Thailand'
  },
  {
    mountain: 'Mount Diablo',
    metres: '1,173',
    range: '',
    location: 'California, US'
  },
  { mountain: 'Mount Hombori', metres: '1,155', range: '', location: 'Mali' },
  { mountain: 'Lochnagar', metres: '1,155', range: '', location: 'Scotland' },
  {
    mountain: 'Bidean nam Bian',
    metres: '1,150',
    range: '',
    location: 'Scotland'
  },
  {
    mountain: 'Monte Musinè',
    metres: '1,150',
    range: '',
    location: 'Piedmont, Italy'
  },
  {
    mountain: 'Brocken',
    metres: '1,141',
    range: '',
    location: 'Saxony-Anhalt, Germany'
  },
  { mountain: 'Victoria Peak', metres: '1,120', range: '', location: 'Belize' },
  {
    mountain: 'Gora Sovetskaya',
    metres: '1,096',
    range: '',
    location: 'Wrangel Island, Chukotka'
  },
  {
    mountain: 'Kayser Mountain',
    metres: '1,094',
    range: '',
    location: 'Greenland, Denmark'
  },
  {
    mountain: 'Mount Juneau',
    metres: '1,090',
    range: '',
    location: 'Alaska, US'
  },
  {
    mountain: 'Snowdon',
    metres: '1,085',
    range: 'Gwynedd, Wales',
    location: 'United Kingdom'
  },
  {
    mountain: 'Tafelberg',
    metres: '1,085',
    range: '',
    location: 'South Africa'
  },
  {
    mountain: 'Flattop Mountain',
    metres: '1,070',
    range: '',
    location: 'Alaska, US'
  },
  { mountain: 'Uummannaq', metres: '1,070', range: '', location: 'Greenland' },
  {
    mountain: 'White Butte',
    metres: '1,069',
    range: '',
    location: 'North Dakota, US'
  },
  {
    mountain: 'Hogback Mountain',
    metres: '1,059',
    range: '',
    location: 'Virginia, US'
  },
  {
    mountain: 'Liathach',
    metres: '1,055',
    range: 'Torridon',
    location: 'Scotland'
  },
  {
    mountain: 'Saka Haphong',
    metres: '1,052',
    range: 'Bandarban',
    location: 'Bangladesh'
  },
  {
    mountain: 'Cambirela',
    metres: '1,043',
    range: 'Palhoça',
    location: 'Brazil'
  },
  {
    mountain: 'Corrán Tuathail',
    metres: '1,038',
    range: 'County Kerry',
    location: 'Ireland'
  },
  {
    mountain: 'Mount Ramon',
    metres: '1,037',
    range: 'Negev',
    location: 'Israel'
  },
  { mountain: 'Girnar', metres: '1,031', range: 'Gujarat', location: 'India' },
  { mountain: 'Mount Zagora', metres: '1,030', range: '', location: 'Morocco' },
  {
    mountain: 'Buachaille Etive Mor',
    metres: '1,022',
    range: 'Glen Etive',
    location: 'Scotland'
  },
  {
    mountain: 'Munboksan',
    metres: '1,015',
    range: '',
    location: 'South Korea'
  },
  { mountain: 'Kékes', metres: '1,014', range: '', location: 'Hungary' },
  {
    mountain: 'Mount Belumut',
    metres: '1,010',
    range: 'Johor',
    location: 'Malaysia'
  },
  {
    mountain: 'Old Rag Mountain',
    metres: '1,001',
    range: '',
    location: 'Virginia, US'
  },
  { mountain: 'Magura Priei', metres: '996', range: '', location: 'Romania' },
  {
    mountain: 'Sgurr Dearg',
    metres: '986',
    range: 'Cuillin',
    location: 'Scotland'
  },
  {
    mountain: 'Mount Sizer',
    metres: '980',
    range: 'Diablo Range',
    location: 'US (California)'
  },
  {
    mountain: 'Mount Valin',
    metres: '980',
    range: 'Saguenay Lac St-Jean',
    location: 'Canada (Québec)'
  },
  {
    mountain: 'Hyangnosan',
    metres: '979',
    range: '',
    location: 'Gyeongnam Province, South Korea'
  },
  {
    mountain: 'Scafell Pike',
    metres: '978',
    range: 'Southern Fells',
    location: 'England (Cumbria)'
  },
  {
    mountain: 'Mount Edgecumbe',
    metres: '976',
    range: ' ',
    location: 'US (Alaska)'
  },
  {
    mountain: 'Grand Bonhomme',
    metres: '973',
    range: '',
    location: 'Saint Vincent and the Grenadines'
  },
  {
    mountain: 'North Mountain (Catskills)',
    metres: '969',
    range: 'Catskill Escarpment',
    location: 'US (New York)'
  },
  {
    mountain: 'Doli Gutta',
    metres: '965',
    range: 'Deccan Plateau',
    location: 'Telangana State, India'
  },
  {
    mountain: 'Mount Monadnock',
    metres: '965',
    range: '',
    location: 'US (New Hampshire)'
  },
  {
    mountain: 'Mount Pirongia',
    metres: '959',
    range: 'Hakarimata Range',
    location: 'New Zealand'
  },
  { mountain: 'Tai Mo Shan', metres: '957', range: '', location: 'Hong Kong' },
  {
    mountain: 'Chimneytop',
    metres: '950.1',
    range: 'Bays Mountain Range',
    location: 'Tennessee, US'
  },
  {
    mountain: 'Helvellyn',
    metres: '950',
    range: 'Eastern Fells',
    location: 'England (Cumbria)'
  },
  {
    mountain: 'Mount Gimie',
    metres: '950',
    range: '',
    location: 'Saint Lucia'
  },
  {
    mountain: 'El Cerro del Aripo',
    metres: '940',
    range: 'Northern Range',
    location: 'Trinidad and Tobago'
  },
  {
    mountain: 'El Tucuche',
    metres: '936',
    range: 'Northern Range',
    location: 'Trinidad and Tobago'
  },
  { mountain: 'Lantau Peak', metres: '934', range: '', location: 'Hong Kong' },
  {
    mountain: 'Betlingchhip',
    metres: '930',
    range: 'Jampui Hills',
    location: 'India'
  },
  {
    mountain: 'Kaimondake volcano',
    metres: '924',
    range: '',
    location: 'Kagoshima, Japan'
  },
  {
    mountain: 'San Salvatore',
    metres: '912',
    range: '',
    location: 'Ticino, Switzerland'
  },
  {
    mountain: 'Pantokrator',
    metres: '906',
    range: '',
    location: 'Greece (Corfu)'
  },
  {
    mountain: 'Edinburgh Peak',
    metres: '904',
    range: '',
    location: 'Tristan Da Cunha, UK'
  },
  { mountain: 'Cadair Idris', metres: '893', range: '', location: 'Wales' },
  { mountain: 'Pen y Fan', metres: '886', range: '', location: 'Wales' },
  {
    mountain: 'Baekunsan',
    metres: '885',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Mount Gerizim',
    metres: '881',
    range: '',
    location: 'West Bank'
  },
  { mountain: 'Sunset Peak', metres: '869', range: '', location: 'Hong Kong' },
  {
    mountain: 'Mahuli',
    metres: '858',
    range: '',
    location: 'Thane District, Maharashtra, India'
  },
  {
    mountain: 'Mount Taylor',
    metres: '856',
    range: '',
    location: 'Canberra, Australia'
  },
  {
    mountain: 'Slieve Donard',
    metres: '856',
    range: 'Mourne Mountains',
    location: 'County Down, Northern Ireland'
  },
  {
    mountain: 'Tinakula',
    metres: '851',
    range: 'Tinakula',
    location: 'Solomon Islands'
  },
  {
    mountain: 'Muntele Cetatuia',
    metres: '850',
    range: '',
    location: 'Romania'
  },
  {
    mountain: 'Mount Saint Catherine',
    metres: '840',
    range: '',
    location: 'Grenada'
  },
  {
    mountain: 'Mount Magazine',
    metres: '839',
    range: 'Ozark Mountains',
    location: 'Arkansas, US'
  },
  {
    mountain: 'Hoemunsan',
    metres: '837',
    range: '',
    location: 'North Jeolla Province, South Korea'
  },
  {
    mountain: 'Bukhansan (Baegundae Peak)',
    metres: '836.5',
    range: '',
    location: 'Seoul, South Korea'
  },
  { mountain: 'Drocea', metres: '836', range: '', location: 'Romania' },
  {
    mountain: 'Piton de la Petite Rivière Noire',
    metres: '828',
    range: '',
    location: 'Black River Mountain Range, Mauritius'
  },
  { mountain: 'Pieter Both', metres: '820', range: '', location: 'Mauritius' },
  {
    mountain: 'Mount Carleton',
    metres: '817',
    range: 'Appalachian Mountains',
    location: 'Canada (New Brunswick)'
  },
  {
    mountain: 'The Cheviot',
    metres: '815',
    range: 'Cheviot Hills',
    location: 'England (Northumberland)'
  },
  { mountain: 'Le Pouce', metres: '811', range: '', location: 'Mauritius' },
  {
    mountain: 'Mount Santubong',
    metres: '810',
    range: '',
    location: 'Malaysia (Sarawak)'
  },
  {
    mountain: 'Gros Morne',
    metres: '807',
    range: '',
    location: 'Canada (Newfoundland)'
  },
  {
    mountain: 'Clisham',
    metres: '799',
    range: '',
    location: 'Harris, Western Isles, Scotland'
  },
  {
    mountain: 'Mount Tamalpais',
    metres: '792',
    range: 'California Coast Ranges',
    location: 'US (California)'
  },
  {
    mountain: 'Mission Peak',
    metres: '790',
    range: 'Diablo Range',
    location: 'US (California)'
  },
  {
    mountain: 'Gyemyeongsan',
    metres: '774',
    range: '',
    location: 'North Chungcheong Province, South Korea'
  },
  {
    mountain: 'Mount Boucherie',
    metres: '758',
    range: '',
    location: 'Canada (British Columbia)'
  },
  { mountain: 'Daeunsan', metres: '742', range: '', location: 'South Korea' },
  {
    mountain: 'Mount Dobong',
    metres: '740',
    range: '',
    location: 'South Korea'
  },
  {
    mountain: 'Mount Lofty',
    metres: '727',
    range: '',
    location: 'South Australia'
  },
  { mountain: 'Ben Cleuch', metres: '721', range: '', location: 'Scotland' },
  {
    mountain: 'Corps de Garde',
    metres: '720',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Gaseopsan',
    metres: '710',
    range: '',
    location: 'North Chungcheong Province, South Korea'
  },
  {
    mountain: 'Mount Dick',
    metres: '705',
    range: '',
    location: 'Auckland Islands, New Zealand'
  },
  {
    mountain: 'Piton Savanne',
    metres: '704',
    range: '',
    location: 'Mauritius'
  },
  { mountain: 'Pu‘u ‘Ō‘ō', metres: '698', range: '', location: 'US (Hawaii)' },
  {
    mountain: 'Signal de Botrange',
    metres: '694',
    range: 'High Fens',
    location: 'Belgium (Liège)'
  },
  {
    mountain: 'Ishpatina Ridge',
    metres: '690',
    range: '',
    location: 'Canada (Ontario)'
  },
  {
    mountain: 'Curepipe Point',
    metres: '686',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Galgisan',
    metres: '685',
    range: '',
    location: 'Gyeonggi Province and Gangwon Province, South Korea'
  },
  {
    mountain: 'Delphi',
    metres: '681',
    range: '',
    location: 'Greece (Skopelos)'
  },
  {
    mountain: 'Qalorujoorneq',
    metres: '676',
    range: '',
    location: 'Greenland'
  },
  {
    mountain: "Lion's Head",
    metres: '669',
    range: '',
    location: 'Cape Town, South Africa'
  },
  {
    mountain: 'Les Trois Mamelles',
    metres: '666',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Steling',
    metres: '658',
    range: 'High Fens',
    location: 'Belgium'
  },
  {
    mountain: 'Maple Mountain',
    metres: '642',
    range: '',
    location: 'Canada (Ontario)'
  },
  {
    mountain: 'Mountain Lagrave (Montage Lagrave)',
    metres: '638',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Kinder Scout',
    metres: '636',
    range: '',
    location: 'Peak District, England'
  },
  { mountain: 'Masaya', metres: '635', range: '', location: 'Nicaragua' },
  {
    mountain: 'Calebasses Mountain (Montagne Calebasses)',
    metres: '632',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: "Mount D'Urville",
    metres: '630',
    range: '',
    location: 'Auckland Islands, New Zealand'
  },
  {
    mountain: 'Moel Eilio',
    metres: '629',
    range: '',
    location: 'Snowdonia, Wales'
  },
  {
    mountain: 'High Willhays',
    metres: '621',
    range: '',
    location: 'England (Dartmoor)'
  },
  {
    mountain: 'Snaefell',
    metres: '621',
    range: '',
    location: 'Isle of Man, British Isles'
  },
  {
    mountain: 'Beinn Mhòr',
    metres: '620',
    range: '',
    location: 'South Uist, United Kingdom'
  },
  {
    mountain: 'Munsusan (Ulsan)',
    metres: '600',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Mount Takao',
    metres: '599',
    range: '',
    location: 'Tokyo, Japan'
  },
  {
    mountain: 'Piton de Fouge',
    metres: '596',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Sikjangsan',
    metres: '596',
    range: '',
    location: 'Daejeon, South Korea'
  },
  {
    mountain: 'Forbordsfjellet',
    metres: '590',
    range: '',
    location: 'Nord-Trøndelag, Norway'
  },
  {
    mountain: 'Piton du Millieu',
    metres: '589',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Gwanggyosan',
    metres: '582',
    range: '',
    location: 'Gyeonggi Province, South Korea'
  },
  {
    mountain: 'Mount Pond',
    metres: '576',
    range: '',
    location: 'South Shetland Islands'
  },
  {
    mountain: 'Mount Tabor',
    metres: '575',
    range: '',
    location: 'Galilee, Israel'
  },
  {
    mountain: 'Monte Conero',
    metres: '572',
    range: '',
    location: 'Italy (Ancona)'
  },
  {
    mountain: 'Gyeryongsan',
    metres: '566',
    range: '',
    location: 'South Gyeongsang Province, South Korea'
  },
  {
    mountain: 'Mount Honey',
    metres: '558',
    range: '',
    location: 'Campbell Islands, New Zealand'
  },
  {
    mountain: 'Le Morne Brabant',
    metres: '555',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Tourelle du Tamarin',
    metres: '548',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Rempart Mountain (Montagne du Rempart)',
    metres: '545',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Palasip Qaqqaa (Præstefjeldet)',
    metres: '544',
    range: '',
    location: 'Qeqqata, Greenland'
  },
  {
    mountain: 'Namamsan',
    metres: '543',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Taum Sauk Mountain',
    metres: '540',
    range: '',
    location: 'Missouri, US'
  },
  {
    mountain: 'Great Mell Fell',
    metres: '537',
    range: 'Eastern Fells',
    location: 'England (Cumbria)'
  },
  {
    mountain: 'Geomdansan (Seongnam)',
    metres: '534.7',
    range: '',
    location: 'Gyeonggi Province, South Korea'
  },
  {
    mountain: 'Mountain Blanche (Montagne Blanche)',
    metres: '532',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Yeonhwasan',
    metres: '532',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Heukseongsan',
    metres: '519',
    range: '',
    location: 'North Chungcheong Province, South Korea'
  },
  { mountain: 'Avala', metres: '511', range: '', location: 'Serbia' },
  {
    mountain: 'Mountain La Terre (Mont La Terre)',
    metres: '504',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Lion Mountain (Montagne Lion)',
    metres: '480',
    range: '',
    location: 'Mauritius'
  },
  {
    mountain: 'Little Si',
    metres: '480',
    range: 'Cascade Range',
    location: 'Washington, US'
  },
  {
    mountain: 'Mount Ramsay',
    metres: '475',
    range: '',
    location: 'Antarctica'
  },
  { mountain: 'Tutuiatu Peak', metres: '467', range: '', location: 'Romania' },
  {
    mountain: 'Mount Keira',
    metres: '462',
    range: '',
    location: 'New South Wales, Australia'
  },
  {
    mountain: "Pu'u Moaulanui",
    metres: '452',
    range: '',
    location: 'Kahoolawe, Hawaii'
  },
  {
    mountain: 'Blue Mountain',
    metres: '452',
    range: '',
    location: 'Ontario, Canada'
  },
  {
    mountain: 'Muryongsan (Ulsan)',
    metres: '452',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Achladokambos',
    metres: '450',
    range: '',
    location: 'Achadokambos, Greece'
  },
  {
    mountain: 'Dongdaesan (Ulsan)',
    metres: '447',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Diamond Hill',
    metres: '442',
    range: 'Connemara National Park',
    location: 'Ireland'
  },
  { mountain: 'Gibraltar', metres: '426', range: '', location: 'Gibraltar' },
  {
    mountain: 'Pic Paradis',
    metres: '424',
    range: '',
    location: 'Saint Martin (France)'
  },
  {
    mountain: 'Havsula',
    metres: '415',
    range: '',
    location: 'Svalbard, Norway'
  },
  {
    mountain: 'Storm King Mountain',
    metres: '408',
    range: 'Hudson Highlands',
    location: 'US (New York)'
  },
  {
    mountain: 'Gyeyangsan',
    metres: '395',
    range: '',
    location: 'Incheon, South Korea'
  },
  {
    mountain: 'Mount Galloway',
    metres: '366',
    range: '',
    location: 'Antipode Islands, New Zealand'
  },
  {
    mountain: 'Pawala Valley Ridge',
    metres: '347',
    range: '',
    location: 'Pitcairn Islands (United Kingdom)'
  },
  {
    mountain: 'Loughrigg Fell',
    metres: '335',
    range: 'Central Fells',
    location: 'England (Cumbria)'
  },
  {
    mountain: 'Mount Kinka (Gifu)',
    metres: '329',
    range: '',
    location: 'Gifu, Japan'
  },
  {
    mountain: 'Vaalserberg',
    metres: '321',
    range: '',
    location: 'Netherlands (Limburg)'
  },
  {
    mountain: 'Mount Bates',
    metres: '319',
    range: 'Norfolk Island, Australia',
    location: ''
  },
  {
    mountain: 'Suur Munamägi',
    metres: '318',
    range: '',
    location: 'Haanja, Estonia'
  },
  { mountain: 'Gaiziņkalns', metres: '312', range: '', location: 'Latvia' },
  { mountain: 'Mutla Ridge', metres: '306', range: '', location: 'Kuwait' },
  {
    mountain: 'Aukštojas',
    metres: '293.84',
    range: '',
    location: 'Medininkai, Lithuania'
  },
  {
    mountain: 'Kruopinė',
    metres: '293.65',
    range: '',
    location: 'Vilnius, Lithuania'
  },
  {
    mountain: 'Juozapinė',
    metres: '292.7',
    range: '',
    location: 'Vilnius, Lithuania'
  },
  {
    mountain: 'Morne du Vitet',
    metres: '286',
    range: '',
    location: 'Saint Barthélemy (France)'
  },
  {
    mountain: 'Qixia Mountain',
    metres: '286',
    range: '',
    location: 'Nanjing, China'
  },
  {
    mountain: 'Hwajangsan',
    metres: '285',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Potolaka/Putuo Shan',
    metres: '284',
    range: '',
    location: 'Zhejiang, China'
  },
  {
    mountain: 'Westmacott',
    metres: '253',
    range: '',
    location: 'New South Wales, Australia'
  },
  {
    mountain: 'Jerimoth Hill',
    metres: '247',
    range: '',
    location: 'Rhode Island, US'
  },
  {
    mountain: 'Mount Ngerchelchuus',
    metres: '242',
    range: '',
    location: 'Babeldaob, Palau'
  },
  {
    mountain: 'Diamond Head',
    metres: '232',
    range: '',
    location: 'US (Hawaii)'
  },
  {
    mountain: 'Yeomposan',
    metres: '203',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Hamwolsan',
    metres: '200',
    range: '',
    location: 'Ulsan, South Korea'
  },
  {
    mountain: 'Great Blue Hill',
    metres: '193.5',
    range: '',
    location: 'Massachusetts, US'
  },
  { mountain: 'Alto de Coloane', metres: '170', range: '', location: 'Macau' },
  {
    mountain: 'Bukit Timah Hill',
    metres: '164',
    range: '',
    location: 'Singapore'
  },
  {
    mountain: 'Himmelbjerget',
    metres: '147',
    range: '',
    location: 'Silkeborg (Denmark)'
  },
  {
    mountain: 'Les Platons',
    metres: '143',
    range: '',
    location: 'Jersey (United Kingdom)'
  },
  {
    mountain: 'Aborrebjerg',
    metres: '143',
    range: '',
    location: 'Møn, Denmark'
  },
  {
    mountain: 'Māngere Mountain',
    metres: '107',
    range: '',
    location: 'New Zealand'
  },
  {
    mountain: 'Britton Hill',
    metres: '105',
    range: '',
    location: 'Florida, US'
  },
  {
    mountain: 'Qurayn Abu al Bawl',
    metres: '103',
    range: '',
    location: 'Qatar'
  },
  { mountain: 'Command Ridge', metres: '71', range: '', location: 'Nauru' },
  {
    mountain: 'Mount Wycheproof',
    metres: '43',
    range: '',
    location: 'Victoria, Australia'
  },
  {
    mountain: 'Hoge Blekker',
    metres: '35',
    range: '',
    location: 'West Flanders, Belgium'
  },
  {
    mountain: 'Bentenyama',
    metres: '6.1',
    range: '',
    location: 'Tokushima, Tokushima Prefecture, Japan'
  },
  { mountain: 'Mount Pleasant', metres: '5', range: '', location: 'Texas, US' },
  {
    mountain: 'Mount Tenpō',
    metres: '4.53',
    range: '',
    location: 'Osaka, Japan'
  },
  {
    mountain: 'Tianzhong Mountain',
    metres: '3.6',
    range: '',
    location: 'Henan Province, China'
  },
  {
    mountain: 'Mount Hiroyama',
    metres: '3',
    range: '',
    location: 'Sendai, Japan'
  },
  {
    mountain: 'Jing Mountain [yue]',
    metres: '0.6',
    range: '',
    location: 'Shandong Province, China'
  }
];

export { mountains };
