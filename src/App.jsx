import React, { useState, useRef } from 'react';
import fuzzysort from 'fuzzysort';
import { mountains } from '../data/mountains';
import './styles.css';

const data = mountains.map((m, i) => {
  return {
    mountainText: `${m.mountain} ${m.range} ${m.location}`,
    mountainIndex: i
  };
});

const mountainText = (i) => {
  return `${mountains[i].mountain}, ${mountains[i].metres} meters, ${
    mountains[i].range
  } ${mountains[i].location}`;
};

const App = () => {
  const [results, setResults] = useState([]);
  const [isDescriptionVisible, setIsDescriptionVisible] = useState(true);
  const inputRef = useRef(null);
  const resultsHeadingRef = useRef(null);

  const findResults = (text) => {
    const results = fuzzysort
      .go(text, data, {
        key: 'mountainText',
        limit: 30, // max results
        allowTypo: false,
        threshold: -10000
      })
      .map((v) => v.obj.mountainIndex);

    setResults(results);
  };

  const handleInputOnChange = (event) => {
    if (event.target.value === '') {
      setIsDescriptionVisible(true);
      setResults([]);
    } else {
      findResults(event.target.value);
      setIsDescriptionVisible(false);
    }
  };

  const handleInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      resultsHeadingRef.current.focus();
    }
  };

  const handleResultsHeadingKeyUp = (event) => {
    if (event.key === 'Escape') {
      inputRef.current.focus();
    }
  };

  return (
    <main className="container">
      <search role="search">
        <h1>
          <label htmlFor="field">Find a mountain ⛰️</label>
        </h1>
        <p>
          This is a Reactjs version of{' '}
          <a href="https://www.scottohara.me/blog/2022/02/05/dynamic-results.html">
            Scott O'Hara's suggestion on handling dynamic search results
          </a>{' '}
          in an accessible way.<br />
          You can{' '}
          <a href="https://www.gitlab.com/pragalakis/react-dynamic-search-results">
            check the code on Gitlab
          </a>.
        </p>
        <input
          id="field"
          ref={inputRef}
          onChange={handleInputOnChange}
          onKeyUp={handleInputKeyUp}
          aria-describedby="description"
        />
        <h3
          tabIndex="-1"
          ref={resultsHeadingRef}
          onKeyUp={handleResultsHeadingKeyUp}
        >
          Results:{' '}
          <span
            style={
              results.length > 0
                ? { visibility: 'visible' }
                : { visibility: 'hidden' }
            }
          >
            <small>{results.length} available</small>
          </span>
        </h3>
        {isDescriptionVisible && <div>The results will update as you type</div>}
        <div>
          {results.length > 0 && (
            <ul>
              {results.map((result, i) => (
                <li key={i}>{mountainText(result)}</li>
              ))}
            </ul>
          )}
        </div>
        <div aria-live="assertive" aria-atomic="true">
          {results.length === 0 &&
            !isDescriptionVisible && <>No results found!</>}
        </div>
      </search>
    </main>
  );
};

export { App };
