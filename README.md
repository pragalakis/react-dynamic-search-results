# React dynamic search results

## [Check the website](https://pragalakis.gitlab.io/react-dynamic-search-results)

![react dynamic search results screenshot](./react-dynamic-search-results-screenshot.png)

This is a Reactjs version of [Scott O'Hara's article](https://www.scottohara.me/blog/2022/02/05/dynamic-results.html).

It uses a fuzzy search library to get the results as you type and announces them in an accessible way.

## How to run it locally

- Clone this repo
- Install all the dependencies: `npm install`
- Run the site locally: `npm start`
